#!/bin/bash

set -e

mkdir -p out

# Create cropped images
convert -crop 33x33  ./494-649.png out/2-%03d.png
convert -crop '11x1-1-0@!' ./0-493-forms.png out/1-%03d.png
convert -crop 34x35 ./0-493.png out/0-%04d.png

# Delete the second frames from 0-493.png
cd out && ls | grep -P '^0-\d\d\d[13579].png$' | xargs -d"\n" rm && cd ..

# Delete empty images from 494-649.png
rm out/2-34*.png

# Delete the second frames from 494-649.png
cd out && ls | grep -P '^2-\d\d[13579].png$' | xargs -d"\n" rm && cd ..

for image in ./out/0-*.png; do
    mogrify -gravity southeast -chop 2x3 $image
done

for image in ./out/2-*.png; do
    mogrify -gravity northeast -chop 1x1 $image
done

# Mass rename to index numbers (for non-forms)
python3 renamerino.py

rm ./out/s-*.png
cp ./paldump.bin ./out
mv ./out $1
