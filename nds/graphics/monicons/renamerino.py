#!/usr/bin/env python3

import os

def gen5Name(oldnum):
    newnum = (oldnum // 2) + 494
    
    # Basculin's second form is in the main section...
    if (newnum == 551):
        return 'out/550-1.png'
    elif (newnum > 551):
        newnum = newnum - 1
        
    return "out/{0:03d}.png".format(newnum) if newnum <= 649 else "out/s-{0:03d}.png".format(newnum)

for filename in os.listdir('out'):
    if filename.startswith('0-'):
        oldnum = int(filename[2:-4])
        newnum = oldnum // 2
        
        # Missing Aggron
        if (newnum > 305):
            newnum = newnum + 1
            
        # Missing Relicanth
        if (newnum > 367):
            newnum = newnum + 1
            
        # Missing Gible
        if (newnum > 442):
            newnum = newnum + 1
            
        newname = "out/{0:03d}.png".format(newnum) if newnum < 494 else "out/s-{0:03d}.png".format(newnum)
        
        os.rename('out/' + filename, newname)
    elif filename.startswith('1-'):
        os.rename('out/' + filename, 'out/s-' + filename[2:])
        
    elif filename.startswith('2-'):
        oldnum = int(filename[2:-4])
        newname = gen5Name(oldnum)
        os.rename('out/' + filename, newname)
        
for i in range(0, 3):
    os.rename('out/s-00{}.png'.format(i), 'out/351-{}.png'.format(i + 1))
    
os.rename('out/s-003.png', 'out/421-1.png')
    
for i in range(4, 9):
    os.rename('out/s-00{}.png'.format(i), 'out/479-{}.png'.format(i - 3))
    
os.rename('out/s-009.png', 'out/487-1.png')
os.rename('out/s-010.png', 'out/492-1.png')
os.rename('out/s-494.png', 'out/Egg.png')
os.rename('out/s-495.png', 'out/490-Egg.png')

for i in range(496, 499):
    os.rename('out/s-{}.png'.format(i), 'out/386-{}.png'.format(i - 495))

for i in range(500, 527):
    os.rename('out/s-{}.png'.format(i), 'out/201-{}.png'.format(i - 499))
    
for i in range(527, 529):
    os.rename('out/s-{}.png'.format(i), 'out/412-{}.png'.format(i - 526))
    
for i in range(529, 531):
    os.rename('out/s-{}.png'.format(i), 'out/413-{}.png'.format(i - 528))
    
for i in range(531, 533):
    os.rename('out/s-{}.png'.format(i), 'out/{}-1.png'.format(i - 109))
    
os.rename('out/649.png', 'out/648-1.png')
os.rename('out/s-650.png', 'out/649.png')

for i in range(651, 653):
    os.rename('out/s-{}.png'.format(i), 'out/{}-1.png'.format(i - 66))
    
os.rename('out/s-653.png', 'out/585-3.png')
os.rename('out/s-654.png', 'out/521-1.png')
              
for i in range(655, 657):
    os.rename('out/s-{}.png'.format(i), 'out/{}-1.png'.format(i - 63))
              
os.rename('out/s-657.png', 'out/555-1.png')
os.rename('out/s-658.png', 'out/585-2.png')
os.rename('out/s-659.png', 'out/586-3.png')
os.rename('out/s-660.png', 'out/585-1.png')
os.rename('out/s-662.png', 'out/586-1.png')
