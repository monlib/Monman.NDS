"""Just executes uigen

I highly suspect that there's something I am missing, but it seems like
the only way to run uigen.__main__ directly would be to cd into buildscripts/
and python -m uigen or to add an __init__ to buildscripts. Neither of these are
pretty, so I just added this weird script thingy.
"""

import uigen.__main__

uigen.__main__._main()
