"""Tileset first stage processing

This module contains functions to process tilesets without interacting with
tilemaps or palettes.
"""

def indexed_flippable_8bpp(image, palette):
    """Generates an unpatched flippable 8bpp indexed tileset.

    Subpalette indexes are generated for extended palettes.
    """
    return _indexed_flippable(image, palette, subpal_size=256)


def indexed_flippable_4bpp(image, palette):
    """Generates an unpatched flippable 4bpp indexed tileset."""
    return _indexed_flippable(image, palette, subpal_size=16)


def indexed_sprite_8bpp(image, palette):
    """Generates unpatched indexed 8bpp sprite graphics."""
    return _indexed_sprite(image, palette, subpal_size=256)


def indexed_sprite_4bpp(image, palette):
    """Generates unpatched indexed 4bpp sprite graphics."""
    return _indexed_sprite(image, palette, subpal_size=16)


def _indexed_sprite(image, palette, subpal_size):
    raw_tiles = _indexed_tiles(image, palette)
    return _extract_subpals(raw_tiles, subpal_size)


def _indexed_flippable(image, palette, subpal_size):
    raw_tiles = _indexed_tiles(image, palette)
    flip_patch, flip_normalized_tiles = _normalize_flips(raw_tiles)
    subpal_patch, subpal_reduced_tiles = _extract_subpals(flip_normalized_tiles, subpal_size)
    index_patch, reduced_tiles = _reduce_tiles(subpal_reduced_tiles)
    return (index_patch, flip_patch, subpal_patch), reduced_tiles


def _indexed_tiles(image, palette):
    # TODO: Support true color images.
    if image.mode != 'P':
        raise Exception('Non-Indexed images are not yet supported.')

    return _partitioned_as_tiles(list(image.getdata()), image.height, image.width, 8, 8)


def _partitioned_as_tiles(pixels, img_height, img_width, tile_height, tile_width):
    if img_width % tile_width != 0 or img_height % tile_height != 0:
        raise Exception('Image size must be integer multiple of tile size.')

    for vertical_tile_index in range(img_height // tile_height):
        for horizontal_tile_index in range(img_width // tile_width):
            first_pixel_x = horizontal_tile_index * tile_width
            first_pixel_y = vertical_tile_index * tile_height
            tile = []
            for line_y in range(first_pixel_y, first_pixel_y + tile_height):
                pixel_index = line_y * img_width + first_pixel_x
                tile.extend(pixels[pixel_index:pixel_index + tile_width])
            yield tuple(tile)


def _normalize_flips(tiles):
    # TODO: Normalize flips
    return no_flip_change, tiles


def _extract_subpals(tiles, subpal_size):
    subpal_indexes = []
    reduced_tiles = []
    for tile in tiles:
        subpal_index = tile[0] // subpal_size
        subpal_indexes.append(subpal_index)
        reduced_tiles.append(tuple(pixel % subpal_size for pixel in tile))

    return subpal_indexes.__getitem__, reduced_tiles


def _reduce_tiles(tiles):
    # TODO: Reduce tiles
    return no_index_change, tiles


def no_index_change(index):
    return index


def no_flip_change(index):
    return False, False
