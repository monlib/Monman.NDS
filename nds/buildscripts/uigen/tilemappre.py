"""Tilemap first stage processing

This module contains functions to process tilemaps without interacting
with palettes or tilesets.

The tileset_info type used in this module is any type that expands to
(x, int, int) where the second value is the tileset's size and the third
parameter is the first gid of the tileset in the given map. The first value
is the fallback value for all empty cells; it can be of any type.
"""

import itertools

from .errors import NotSupportedOnNDSError


def pregen_text(group_layer, background_tileset_info, subpal_tileset_info):
    """Pregenerates a text (or ExtRot) background layer.
    
    Returns a triple of sequences of indexes, flip_flags, and subpal overrides.
    tileset_info types are documented in the module docstring.
    """
    indexes = _tiled_map_as_indexes(
        group_layer.background.map_entries,
        background_tileset_info,
    )
    flip_flags = _tiled_map_flip_flags(group_layer.background.map_entries)
    subpal_overrides = _tiled_map_as_indexes(
        group_layer.subpal_overrides.map_entries,
        subpal_tileset_info,
    )
    return indexes, flip_flags, subpal_overrides


def pregen_rot(group_layer, tileset_info):
    """Pregenerates a rotation background layer.

    Returns a sequence of indexes into the tileset.
    The tileset_info type is documented in the module docstring.
    """
    return _tiled_map_as_indexes(group_layer.background.map_entries, tileset_info)


def _tiled_map_as_indexes(tilemap, tileset_info):
    return (
        _entry_index_useful_exception(i, entry, tileset_info)
        for i, entry in enumerate(tilemap)
    )


def _entry_index_useful_exception(i, entry, tileset_info):
    try:
        return _entry_index(entry, *tileset_info)
    except Exception as e:
        raise NotSupportedOnNDSError((
            'Tilemap cannot contain different tilesets!\n'
            'Offending tile at index {} with value {}').format(i, entry)
        ) from e


def _entry_index(entry_int, default_index, tileset_size, first_gid):
    """Extracts the tile index from a tilemap entry, converting 0 to default_index.

    Non-defaulted indexes are tested to be in [0; max_index) and an exception is raised
    if they aren't.
    """
    index_bits = ~(0b111 << 29)
    index = (entry_int & index_bits) - first_gid if entry_int != 0 else default_index
    if index != default_index and index not in range(tileset_size):
        raise Exception('Tile is not in correct tilemap!')

    return index


def _tiled_map_flip_flags(tilemap):
    return (_flip_flags_useful_exception(i, tile) for i, tile in enumerate(tilemap))


def _flip_flags_useful_exception(i, tile):
    try:
        return _flip_flags(tile)
    except NotSupportedOnNDSError as e:
        raise NotSupportedOnNDSError((
            'Tilemap contains diagonal flipping which is not possible on the nds!\n'
            'Offending tile at index {}').format(i)) from e


def _flip_flags(entry_int):
    """Converts a Tiled tilemap entry into a tuple of hflip and vflip"""
    hflip = entry_int & (1 << 31) != 0
    vflip = entry_int & (1 << 30) != 0
    dflip = entry_int & (1 << 29) != 0

    if dflip:
        raise NotSupportedOnNDSError('Diagonal flip is not supported!')

    return (hflip, vflip,)
