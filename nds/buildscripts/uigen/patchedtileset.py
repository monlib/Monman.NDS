"""Tileset second stage processing

This module contains functions to patch tilesets with new palettes and
convert them to bytes.
"""

def tileset_8bpp_bytes(tileset):
    """Converts an 8bpp tileset to its binary representation."""
    return b''.join(
        b''.join(pixel.to_bytes(1, byteorder='little') for pixel in tile)
        for tile in tileset
    )

def tileset_4bpp_bytes(tileset):
    """Converts a 4bpp tileset into its binary representation."""
    return b''.join(
        b''.join((col0 | col1 << 4).to_bytes(1, byteorder='little')
        for col0, col1 in zip(tile[0::2], tile[1::2]))
        for tile in tileset
    )
