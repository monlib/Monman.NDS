"""Utility"""

def rgba16(r, g, b, a=False):
    def five_bit(x):
        return (x // 8) & 0b11111
    return (a is True or (a is not False and a < 255)) << 15 | five_bit(r) | five_bit(g) << 5 | five_bit(b) << 10


def group_3(in_list):
    """[0, 1, 2, 3, 4, 5,] -> [(0, 1, 2,), (3, 4, 5,)]"""
    curr_triple = None
    for i, x in enumerate(in_list):
        if i % 3 == 0 and curr_triple is not None:
            yield curr_triple
            curr_triple = None

        if curr_triple is None:
            curr_triple = (x,)
        else:
            curr_triple += (x,)