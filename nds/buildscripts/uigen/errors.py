class NotSupportedOnNDSError(Exception):
    """Feature is not supported on nds."""
    pass
