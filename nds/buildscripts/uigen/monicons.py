"""Monicon processing"""

import struct
import itertools

from .unpatchedtileset import indexed_sprite_4bpp
from .patchedtileset import tileset_4bpp_bytes
from .util import group_3, rgba16

def extract_rgba16_subpals(paldump_path):
    paldump_bin = paldump_path.read_bytes()
    subpaldumps = [paldump_bin[i:i+32] for i in range(0, len(paldump_bin), 32)]
    return [[int.from_bytes(subpaldump[i:i+2], byteorder='little') for i in range(0, len(subpaldump), 2)] for subpaldump in subpaldumps]


def brief_mon_info(special_filename_match):
    species = int(special_filename_match.group(1))
    form = int(special_filename_match.group(2)) if special_filename_match.group(2).isdigit() else 0
    return (species, form)


def complete_monicon_bytes(image, subpals):
    if image.mode != 'P':
        raise Exception('Mode {} is not supported!'.format(image.mode))
    saved_rgb15_pal = [rgba16(*c, False) for c in group_3(image.getpalette())]
    subpal_index, subpal = _get_matching_subpal(image, saved_rgb15_pal, subpals)
    fixed_image = _remap_image_to_subpal(image, saved_rgb15_pal, subpal)
    return (_subpal_bytes(subpal_index), _monicon_bytes(subpal_index, fixed_image))


def _get_matching_subpal(image, saved_rgb15_pal, subpals):
    used_colors = dict(c[::-1] for c in image.getcolors())
    filtered_palette = [c for i, c in enumerate(saved_rgb15_pal) if i in used_colors]
    visible_palette = filtered_palette[1:]
    return next((i, p) for (i, p) in enumerate(subpals) if set(visible_palette) <= set(p))


def _remap_image_to_subpal(image, saved_rgb15_pal, subpal):
    palette_transform = [0] + [_index_or(saved_rgb15_pal, c, 16) for c in subpal[1:]]
    return image.remap_palette(palette_transform)


def _index_or(inlist, item, default):
    return inlist.index(item) if item in inlist else default


def _monicon_bytes(subpal_index, fixed_image):
    return tileset_4bpp_bytes(indexed_sprite_4bpp(fixed_image, None)[1])


def _subpal_bytes(subpal_index):
    return subpal_index.to_bytes(4, byteorder='little')


def write_monicons(output_path, regular, special, egg):
    regular_count = max(regular) + 1 # max + 1 instead of len, because there are holes
    icon_count = regular_count + len(special) + 1 # includes 1 egg sprite
    metadata_header = struct.Struct('<ii')
    brief_mon_info = struct.Struct('<ii')

    with output_path.joinpath('m').open('wb') as metadata_file:
        metadata_file.write(metadata_header.pack(icon_count, regular_count))
        for info in sorted(special):
            metadata_file.write(brief_mon_info.pack(*info))

    def ordered_graphics():
        regular_values = (regular[i] if i in regular else regular[0] for i in range(regular_count))
        special_values = (v for k, v in sorted(special.items()))
        return itertools.chain(regular_values, special_values, [egg])

    with output_path.joinpath('d').open('wb') as gfx_file:
        gfx_file.write(b''.join(subpal + tiles for subpal, tiles in ordered_graphics()))
