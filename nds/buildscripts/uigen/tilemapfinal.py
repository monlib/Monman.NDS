"""Tilemap second stage processing

This module contains functions to apply changes due to palette and tileset
optimization to the results of first stage processing.
"""

import itertools

from .errors import NotSupportedOnNDSError

def finalize_rot(indexes, index_patch):
    """Finalizes processing of a rotation background by patching all indexes
    
    The function index_patch is applied to each element in indexes and bytes are
    generated from the result. If indexes is an iterator it is fully consumed.
    """
    return _tilemap_rot_bytes(_patched_indexes(indexes, index_patch))


def finalize_text(indexes, flips, subpal_overrides, index_patch, flip_patch, subpal_defaults):
    """Finalizes processing of a text (or ExtRot) background by patching it.

    The function index_patch is applied to each index in the tilemap to get
    the new index in the optimized tileset. The function flip_patch is applied
    to each index in the tilemap (unpatched) to get the flip flags of that tile
    and the result is XOR'd with the corresponding element in flips.
    Subpal_defaults is applied to each index in the tilemap (unpatched) where
    subpal_overrides is none to produce the default subpalette index.
    The result of these patches is used to generate the actual bytes of the tilemap.

    The first three parameters may be iterators; they are assumed to be of
    equal length and will be consumed.
    """
    indexes0, indexes1, indexes2 = itertools.tee(indexes, 3)
    final_indexes = _patched_indexes(indexes0, index_patch)
    final_flips = _patched_flips(flips, indexes1, flip_patch)
    final_subpals = _patched_subpals(subpal_overrides, indexes2, subpal_defaults)
    final_tilemap = _tilemap_text(final_indexes, final_flips, final_subpals)
    return _tilemap_text_bytes(final_tilemap)


def _patched_indexes(indexes, index_patch):
    return (index_patch(index) for index in indexes)


def _patched_flips(flips, tile_indexes, tile_flips):
    return (
        (hflip != tile_flips(index)[0], vflip != tile_flips(index)[1])
        for (index, (hflip, vflip))  in zip(tile_indexes, flips)
    )


def _patched_subpals(overrides, tile_indexes, defaults):
    return (
        override if override is not None else defaults(index)
        for index, override in zip(tile_indexes, overrides)
    )


def _tilemap_text(indexes, flips, subpals):
    return ((index, hflip, vflip, subpal)
        for (index, (hflip, vflip), subpal) in zip(indexes, flips, subpals))


def _tilemap_rot_bytes(tilemap):
    return b''.join(index.to_bytes(1, byteorder='little') for index in tilemap)


def _tilemap_text_bytes(tilemap):
    return b''.join((_text_entry_bytes(*entry) for entry in tilemap))


def _text_entry_bytes(tile_index, hflip, vflip, subpal):
    is_valid = (
        0 <= tile_index < 1024
        and isinstance(hflip, bool)
        and isinstance(vflip, bool)
        and 0 <= subpal < 16
    )
    if is_valid:
        return (tile_index << 0x0 | hflip << 0xA | vflip << 0xB | subpal << 0xC) \
            .to_bytes(2, byteorder='little')
    else:
        raise NotSupportedOnNDSError(
            'Illegal values! {} {} {} {}'.format(tile_index, hflip, vflip, subpal)
        )
