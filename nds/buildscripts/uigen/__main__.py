#!/usr/bin/env python3

"""uigen

Processes UIs for use on the nds.

Usage:
    uigen.py convert [--] <tilemap> <output-folder>
    uigen.py sprite [--] <image> <output-file>
    uigen.py box-bg [--] <image> <output-file>
    uigen.py monicons [--] <paldump> <input-folder> <output-folder>
    uigen.py --help
    uigen.py --version

Options:
    -h --help
        Display this screen

    -v --version
        Show the version number
"""

import itertools
import json
import pathlib
from docopt import docopt
from PIL import Image
import re

from .tiled import TiledTileset, TiledScreenState, TiledTilesetRef
from .errors import NotSupportedOnNDSError
from .tilemappre import pregen_text
from .tilemapfinal import finalize_text
from .unpatchedtileset import indexed_flippable_4bpp, indexed_sprite_4bpp
from .patchedtileset import tileset_4bpp_bytes
from .monicons import brief_mon_info, complete_monicon_bytes, extract_rgba16_subpals, write_monicons
from .util import rgba16, group_3

def _color_matches(color_string, color_tuple):
    return color_string == '{:02X}{:02X}{:02X}'.format(*color_tuple)

def _rgba16_palette(palette, transparent_color):
    return (rgba16(*color, _color_matches(transparent_color, color)) for color in palette)

def _rgba16_palette_bytes(palette):
    return b''.join((color.to_bytes(2, byteorder='little') for color in palette))


def find_tileset(tilemap, tilesets):
    first_tile_index = next(filter(None, tilemap))
    tilesets_smaller_gid = filter(lambda ts: first_tile_index >= ts.first_gid, tilesets)
    tileset_largest_gid = max(tilesets_smaller_gid, key=lambda ts: ts.first_gid)
    return tileset_largest_gid


def path_from_file(src_file_path, dest_path):
    """Converts a path relative to the source file into a path relative to pwd.

    This function assumes that src_file_path is relative to pwd and that dest_path
    is relative to the parent directory of src_file_path.
    """
    return src_file_path.parent / dest_path

def _gen_palette_bytes(input_path, transparent_color):
    with Image.open(str(input_path)) as input_img:
        if input_img.mode != 'P':
            raise Exception('Mode {} is not supported!'.format(input_img.mode))
        ungrouped_palette = input_img.getpalette()

    grouped_palette = group_3(ungrouped_palette)
    halfword_palette = _rgba16_palette(grouped_palette, transparent_color)
    return _rgba16_palette_bytes(halfword_palette)


def _convert_palette(input_path, output_path, transparent_color):
    with output_path.open('wb') as output_file:
        output_file.write(_gen_palette_bytes(input_path, transparent_color))

def _convert_subpalette(input_path, output_path, transparent_color):
    with output_path.open('wb') as output_file:
        output_file.write(_gen_palette_bytes(input_path, transparent_color)[:32])

def _convert_box_bg(input_path, output_path):
    with Image.open(input_path) as input_img:
        _, tileset = indexed_sprite_4bpp(input_img, None)

    with output_path.open('wb') as out_file:
        out_file.write(tileset_4bpp_bytes(tileset))

    palette_output_path = output_path.with_suffix('.pal')
    _convert_subpalette(input_path, palette_output_path, 'FF00FF')

def _convert_monicons(paldump_path, input_path, output_path):
    print("Parsing paldump...")
    subpals = extract_rgba16_subpals(paldump_path)
    print("Converting monicons...")
    regular_icon_bytes = dict()
    special_icon_bytes = dict()
    special_filename_regex = re.compile(r"(\d{3})-(Egg|\d+)")
    egg_icon = None
    for image_path in input_path.glob('*.png'):
        try:
            with Image.open(image_path) as image:
                result_bytes = complete_monicon_bytes(image, subpals)
            special_filename_match = special_filename_regex.match(image_path.stem)
            if image_path.stem == "Egg":
                egg_icon = result_bytes
            elif not special_filename_match:
                regular_icon_bytes[int(image_path.stem)] = result_bytes
            else:
                info = brief_mon_info(special_filename_match)
                special_icon_bytes[info] = result_bytes
        except Exception as exc:
            print("Could not convert {}\n{}\n".format(image_path, exc))
    print('Writing results.')
    write_monicons(output_path, regular_icon_bytes, special_icon_bytes, egg_icon)
    print("Done!")


def _convert_tilemap(input_path, output_path):
    with input_path.open('rt') as tilemap_file:
        tiled_screen_state = TiledScreenState(json.load(tilemap_file))

    for group_layer in tiled_screen_state.group_layers:
        layer_output_path = output_path / (input_path.stem + '-' + group_layer.name)

        # Get background layer
        tiled_tilemap = group_layer.background.map_entries

        # Get tileset
        tileset_ref = find_tileset(tiled_tilemap, tiled_screen_state.tilesets)
        tileset_path = path_from_file(input_path, tileset_ref.source_path)
        with tileset_path.open() as tileset_file:
            tiled_tileset = TiledTileset(json.load(tileset_file))

        # Convert palette
        palette_output_path = layer_output_path.with_suffix('.pal')
        transparent_color = tiled_tileset.transparent_color
        tileset_image_path = path_from_file(tileset_path, tiled_tileset.image_path)
        _convert_palette(tileset_image_path, palette_output_path, transparent_color)

        # Convert tileset
        tileset_output_path = layer_output_path.with_suffix('.tiles')
        with Image.open(str(tileset_image_path)) as input_img:
            patches, unpatched_tilemap = indexed_flippable_4bpp(input_img, None)

        # Convert tilemap
        tilemap_output_path = layer_output_path.with_suffix('.map')
        empty_index = tiled_tileset.blank_tile
        tilecount = tiled_tileset.tilecount
        bg_tileset_info = (empty_index, tilecount, tileset_ref.first_gid)
        # TODO: Don't hardcode this!
        sp_tileset_info = (None, 16, 1025)
        pregened_bg = pregen_text(group_layer, bg_tileset_info, sp_tileset_info)
        tilemap_bytes = finalize_text(*pregened_bg, *patches)

        with tilemap_output_path.open('wb') as tm_output_file:
            tm_output_file.write(tilemap_bytes)

        with tileset_output_path.open('wb') as ts_output_file:
            ts_output_file.write(tileset_4bpp_bytes(unpatched_tilemap))

def _convert_sprite(image_path, output_path):
    with Image.open(image_path) as image:
        _, tileset = indexed_sprite_4bpp(image, None)

    palette_output_path = output_path.with_suffix('.pal')
    _convert_palette(image_path, palette_output_path, 'FF00FF')

    with output_path.open('wb') as output_file:
        output_file.write(tileset_4bpp_bytes(tileset))


def _main():
    args = docopt(__doc__, version='0.1.0')
    if args['convert']:
        _convert_tilemap(pathlib.Path(args['<tilemap>']), pathlib.Path(args['<output-folder>']))
    elif args['sprite']:
        _convert_sprite(pathlib.Path(args['<image>']), pathlib.Path(args['<output-file>']))
    elif args['box-bg']:
        _convert_box_bg(pathlib.Path(args['<image>']), pathlib.Path(args['<output-file>']))
    elif args['monicons']:
        _convert_monicons(pathlib.Path(args['<paldump>']), pathlib.Path(args['<input-folder>']), pathlib.Path(args['<output-folder>']))

if __name__ == '__main__':
    _main()
