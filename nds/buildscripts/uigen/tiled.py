"""Provides classes for interacting with Tiled json files."""

import pathlib

class TiledTileset:
    def __init__(self, json_obj):
        if json_obj['type'] != 'tileset':
            raise Exception('Cannot create TiledTileset with non-tileset!')
        self._json_obj = json_obj

    @property
    def image_path(self):
        return pathlib.Path(self._json_obj['image'])

    @property
    def name(self):
        return self._json_obj['name']

    @property
    def tilecount(self):
        return self._json_obj['tilecount']

    @property
    def _properties(self):
        return self._json_obj['properties']

    @property
    def blank_tile(self):
        return self._properties['BlankTile']

    @property
    def transparent_color(self):
        return self._properties['TransparentColor']


class TiledScreenState():
    def __init__(self, json_data):
        if json_data['type'] != 'map':
            raise Exception('Cannot create TiledTilemap with non-map!')
        self._json_data = json_data

    @property
    def tilesets(self):
        return map(TiledTilesetRef, self._json_data['tilesets'])

    @property
    def group_layers(self):
        return map(TiledGroupLayer, self._json_data['layers'])


class TiledTilesetRef():
    def __init__(self, json_data):
        self._json_data = json_data

    @property
    def first_gid(self):
        return self._json_data['firstgid']

    @property
    def source_path(self):
        return pathlib.Path(self._json_data['source'])


class TiledGroupLayer():
    def __init__(self, json_data):
        self._json_data = json_data

    @property
    def name(self):
        return self._json_data['name']

    @property
    def background(self):
        return TiledMapLayer(self._layer_with_name('Background'))

    @property
    def subpal_overrides(self):
        return TiledMapLayer(self._layer_with_name('Subpalettes'))

    def _layer_with_name(self, name):
        return next(filter(lambda layer: layer['name'] == name, self._json_data['layers']))

class TiledLayer():
    def __init__(self, json_data):
        self._json_data = json_data

    @property
    def name(self):
        return self._json_data['name']

class TiledMapLayer(TiledLayer):
    def __init__(self, json_data):
        super().__init__(json_data)

    @property
    def map_entries(self):
        return self._json_data['data']