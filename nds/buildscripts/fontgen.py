#!/usr/bin/env python3

"""fontgen

Generates usugataPC font files from images and ttf fonts.

Usage:
    fontgen.py images [--] <images-config> <image-directory> <output>
    fontgen.py ttf [--] <ttf-config> <ttf-file> <output>
    fontgen.py --help

Options:
    -h --help
        Display this screen

Using `images`, fontgen assembles a font from a directory of individual png images. The config
is used to configure the palette, spacing, yOffset, and associate glyphs with characters.

Using `ttf`, fontgen converts a ttf font into the usugataPC format. The config stores the spacings,
font scale, and ranges of characters to convert.
"""

import glob
import os
import sys
import collections
import yaml
from collections import namedtuple
from itertools import chain
from docopt import docopt
from PIL import Image, ImageFont, ImageDraw


InputGlyph = namedtuple('InputGlyph', ['image', 'codepoints', 'yOffset', 'spacing'])

def generate_binary(input_glyphs, palette):
    glyph_table, glyph_indexes = generate_glyph_table(palette, input_glyphs)
    unicode_mapper = generate_unicode_mapper(glyph_indexes, input_glyphs)
    return combine_table_and_mapper(glyph_table, unicode_mapper)

def generate_glyph_table(palette, input_glyphs):
    glyph_table = bytearray()
    glyph_indexes = []
    for input_glyph in input_glyphs:
        if len(glyph_table) % 2 != 0:
            glyph_table.extend(bytearray(1))
        glyph_indexes.append(int(len(glyph_table) / 2))
        glyph_table.extend(generate_glyph_table_entry(palette, input_glyph))

    return glyph_table, glyph_indexes

def generate_glyph_table_entry(palette, input_glyph):
    header = generate_glyph_table_entry_header(input_glyph)
    pixels = generate_glyph_table_pixels(palette, input_glyph)
    return combine_to_glyph_table_entry(header, pixels)

def generate_glyph_table_entry_header(input_glyph):
    width, height = input_glyph.image.size
    vertical_offset = input_glyph.yOffset
    spacing = input_glyph.spacing
    return height << 0 | width << 4 | vertical_offset << 8 | spacing << 12

def generate_glyph_table_pixels(palette, input_glyph):
    halfwords = bytearray()
    current_pixel = 0
    current_halfword = 0
    image = input_glyph.image
    for pixel in image.getdata():
        current_halfword |= get_palette_index(palette, pixel) << 2*current_pixel
        current_pixel += 1
        if current_pixel == 8:
            halfwords.extend(current_halfword.to_bytes(2, byteorder='little', signed=False))
            current_halfword = 0
            current_pixel = 0

    # Append the last (unfinished) halfword if the number of pixels is not a multiple
    # of eight.
    if current_pixel != 0:
        halfwords.extend(current_halfword.to_bytes(2, byteorder='little', signed=False))

    return halfwords

def get_palette_index(palette, pixel):
    rgb_string = '{:02X}{:02X}{:02X}'.format(*pixel)
    return palette.index(rgb_string)

def combine_to_glyph_table_entry(header, pixels):
    entry = bytearray()
    entry.extend(header.to_bytes(2, byteorder='little', signed=False))
    entry.extend(pixels)
    return entry

def generate_unicode_mapper(glyph_indexes, input_glyphs):
    mappings = {get_character_bytes(codepoint): i for g, i in zip(input_glyphs, glyph_indexes) for codepoint in g.codepoints}
    return bytearray(chain.from_iterable(chain(
        [len(mappings).to_bytes(4, byteorder='little', signed=False)],
        (chain(codepoint.to_bytes(4, byteorder='little', signed=False), glyph.to_bytes(4, byteorder='little', signed=False))
            for codepoint, glyph in sorted(mappings.items())
        )
    )))

def generate_input_glyphs(config, image_names):
    def glyphs(image_name):
        short_image_name = os.path.splitext(os.path.basename(image_name))[0]
        return (config['glyphs'][short_image_name]['chars'] if short_image_name
            in config['glyphs'] else [short_image_name])
    return [InputGlyph(Image.open(f).convert('RGB'),
        glyphs(f), get_vertical_offset(config, f), get_spacing(config, f))
        for f in image_names]

def get_character_bytes(value):
    """ Converts a character to its codepoint or passes a codepoint through. """
    if type(value) is int:
        return value
    elif type(value) is str and len(value) == 1:
        return ord(value)
    else:
        raise Exception('Could not convert value to codepoint!', value, type(value))

def combine_table_and_mapper(glyph_table, unicode_mapper):
    unicode_mapper.extend(glyph_table)
    return unicode_mapper


def grab_info_from_yaml(config_filename, image_names):
    with open(config_filename, 'r', encoding='utf-8') as config_file:
        config = yaml.load(config_file)
    return generate_input_glyphs(config, image_names), config['palette']

def get_vertical_offset(config, image_name):
    return get_property(config, image_name, 'yOffset') & 0x0F

def get_spacing(config, image_name):
    return get_property(config, image_name, 'spacing') & 0x0F

def get_property(config, image_name, property_name):
    key = os.path.splitext(os.path.basename(image_name))[0]
    if key in config['glyphs'] and property_name in config['glyphs'][key]:
        return config['glyphs'][key][property_name]
    else:
        return config['defaults'][property_name]

def deconstruct_ttf(config_filename, ttf_filename):
    with open(config_filename, 'r', encoding='utf-8') as config_file:
        config = yaml.load(config_file)
    chars = list(requested_chars(config))
    spacings_table = generate_spacings(config)
    spacings = (spacings_table[ord(c)] for c in chars)
    font = ImageFont.truetype(ttf_filename, config['fontscale'])
    generated_glyphs = (image_with_char(c, font) for c in chars)
    in_glyphs = [InputGlyph(i, [c], y, s) for ((i, y), s, c) in zip(generated_glyphs, spacings, chars)]
    palette = ["000000", "FFFFFF"]
    return in_glyphs, palette

def generate_spacings(config):
    default_spacing = config['default_spacing']
    return collections.defaultdict(lambda: default_spacing, config['spacings'])

def requested_chars(config):
    return (c for r in config['characters'] for c in char_range(r['begin'], r['end']))

def char_range(first, last):
    for c in range(ord(first), ord(last) + 1):
        yield chr(c)

def image_with_char(c, font):
    max_glyph_dimensions = (16, 16)
    img = Image.new("RGB", max_glyph_dimensions)
    draw = ImageDraw.Draw(img)
    draw.text((0,0), c, font=font)
    bbox = img.getbbox() or (0, 0, 0, 0)
    img = img.crop(bbox).convert('1').convert("RGB")
    return img, bbox[1]

def write_binary(bin, filename):
    with open(filename, 'wb') as out_file:
        out_file.write(bin)

if __name__ == '__main__':
    args = docopt(__doc__)
    if args['images']:
        image_names = glob.glob(os.path.join(args['<image-directory>'], '*.png'))
        binary = generate_binary(*grab_info_from_yaml(args['<images-config>'], image_names))
        write_binary(binary, args['<output>'])
    elif args['ttf']:
        binary = generate_binary(*deconstruct_ttf(args['<ttf-config>'], args['<ttf-file>']))
        write_binary(binary, args['<output>'])
    else:
        exit(1)
