import os
from conans import ConanFile

class UsugataPcConan(ConanFile):
    name = 'usugataPC'
    version = '0.1.0'
    license = 'GPLv3+'
    url = 'https://gitlab.com/monlib/Monman.NDS.git'
    description = 'legit-use box'
    settings = {'os': ['NDS'], 'build_type': None}
    exports_sources = 'graphics/*', 'buildscripts/*', 'ui/*', '!**/__pycache__', "lua/*", 'credits.txt'
    build_requires = (
        'usugataPC-ARM9/current@monlib/intermediate',
    )

    def build(self):
        cmd = (
            'make -f buildscripts/Makefile '
            '"ARM9ELF={arm9elf}" '
            '"OUTPUT_NDS={output_nds}" "GAME_ICON={game_icon}" '
            '"GAME_DESC={game_desc}" "NDSTOOL={ndstool}" '
            '"FONTGEN={fontgen}" "UIGEN={uigen}" '
            '"DEPS_LUA_DIR={deps_lua_dir}" '
        ).format(
            arm9elf=os.path.join(
                self.deps_cpp_info['usugataPC-ARM9'].rootpath,
                self.deps_cpp_info['usugataPC-ARM9'].bindirs[0],
                'usugataPC'),
            output_nds='usugataPC.nds',
            game_icon='graphics/icon.bmp',
            game_desc="usugata PC;Manage your 'mons!; Here's some text.",
            ndstool='ndstool',
            fontgen='buildscripts/fontgen.py',
            uigen='python3 buildscripts/uigen_runner.py',
            deps_lua_dir=os.path.join(self.deps_cpp_info['usugataPC-ARM9'].rootpath, 'lua')
        )

        self.run(cmd)

    def package(self):
        self.copy('usugataPC.nds', dst='', src='')

    def deploy(self):
        self.copy('usugataPC.nds')
