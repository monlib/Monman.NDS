#include "usugatapc/vm/MainMenuViewModel.h"

#include <gsl/gsl>

#include "usugatapc/vm/SafeLua.h"

namespace usugatapc::vm {
    namespace {}

    MainMenuViewModel::MainMenuViewModel(std::shared_ptr<sol::state> lua,
        sol::table save,
        MonsterManagementFactory makeMonsterManagement) :
        _lua{std::move(lua)},
        _save{std::move(save)},
        _makeMonsterManagement{std::move(makeMonsterManagement)} {
        Ensures(_lua && _makeMonsterManagement);
    }

    std::shared_ptr<MonsterManagementViewModel>
    MainMenuViewModel::openMonsterManager() {
        Expects(_lua);
        auto tableOpt =
            mapResult<sol::table>(luaSafeGet(*_lua, _save, "monsterStorage"));
        if (tableOpt.has_value()) {
            return _makeMonsterManagement->operator()(_lua, std::move(*tableOpt));
        } else {
            return nullptr;
        }
    }

    void MainMenuViewModel::applyMonsterStorageChanges(
        const MonsterManagementViewModel& monMan) {
        auto [primaryStorage] = monMan.getMonsterStorages();
        // call _save.withMonsterStorage(primaryStorage);
        auto withMonsterStorageOpt = mapResult<sol::protected_function>(
            luaSafeGet(*_lua, _save, "withMonsterStorage"));
        if (!withMonsterStorageOpt) {
            throw std::runtime_error("Failed to get Save:withMonsterStorage");
        }
        sol::protected_function_result result =
            withMonsterStorageOpt->call(_save, primaryStorage);
        int retcount = std::as_const(result).return_count();
        if (retcount != 1) {
            throw std::runtime_error("Save:withMonsterStorage failed!");
        }
        _save = result.get<sol::table>(0);
        /// TODO: Reload existing MonsterManagementViewModels
    }
}
