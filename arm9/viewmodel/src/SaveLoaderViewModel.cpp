#include "usugatapc/vm/SaveLoaderViewModel.h"

#include <gsl/gsl>

namespace usugatapc::vm {
    SaveLoaderViewModel::SaveLoaderViewModel(
        std::shared_ptr<sol::state> lua, sol::protected_function loader) :
        _lua{lua},
        _loader{loader} {
        Expects(lua && loader);
    }

    LoadedSave SaveLoaderViewModel::openSave(const char* filename) {
        sol::protected_function_result result = _loader(filename);
        if (result.valid() && sol::object{result}.is<sol::table>()) {
            sol::table save = result;
            return LoadedSave{save};
        } else {
            throw std::runtime_error("Could not load save!");
        }
    }
}
