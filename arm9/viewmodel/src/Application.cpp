#include "usugatapc/vm/Application.h"

#include <gsl/gsl>

namespace usugatapc::vm {
    namespace {}

    Application::Application(const char* monswapPath) :
        _lua{std::make_shared<sol::state>()},
        _getLoaders{nullptr},
        _monsterDetailFactories{std::make_shared<MonsterDetailFactories>()},
        _makeMonsterManagement{std::make_shared<MonsterManagementFactoryImpl>(
            *_lua, monswapPath, _monsterDetailFactories)} {
        openAllLuaLibraries(*_lua);
    }

    SaveLoaderViewModel Application::getSaveLoader() {
        return SaveLoaderViewModel{_lua, _getLoaders(*_lua)};
    }

    void Application::registerLuaModule(
        const std::string& key, ::lua_CFunction luaopen) {
        _lua->require(key, luaopen, false);
    }

    void Application::registerLoaders(
        std::function<sol::protected_function(sol::state_view)> getLoaders) {
        Expects(getLoaders);
        _getLoaders = getLoaders;
    }

    void Application::registerImplicitMigration(std::string_view from,
        std::string_view to,
        sol::function migrateTo,
        sol::function migrateBack) {
        _makeMonsterManagement->registerImplicitMigration(
            from, to, std::move(migrateTo), std::move(migrateBack));
    }

    void Application::registerMonsterDetailViewModel(
        std::string_view format, MonsterDetailFactory f) {
        _monsterDetailFactories->try_emplace(std::string{format}, std::move(f));
    }

    sol::state_view Application::getLua() { return *_lua; }

    std::shared_ptr<MainMenuViewModel> Application::openMainMenu(LoadedSave save) {
        return std::make_shared<MainMenuViewModel>(
            _lua, save.obj, _makeMonsterManagement);
    }
}
