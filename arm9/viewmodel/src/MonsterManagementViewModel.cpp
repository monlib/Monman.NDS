#include "usugatapc/vm/MonsterManagementViewModel.h"

#include <gsl/gsl>

#include "usugatapc/vm/SafeLua.h"

namespace usugatapc::vm {
    namespace {}

    MonsterManagementViewModel::MonsterManagementViewModel(
        std::shared_ptr<sol::state> lua,
        sol::table primaryStorage,
        sol::table monswap,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _lua{std::move(lua)},
        _primaryStorage{_lua, std::move(primaryStorage), monsterDetailFactories},
        _monsterDetailFactories{monsterDetailFactories},
        _monswap{std::move(monswap)},
        _handler{} {
        Ensures(_lua && _monswap && _monsterDetailFactories);
    }

    gsl::not_null<MonsterStorageViewModel*>
    MonsterManagementViewModel::getPrimaryStorage() {
        return &_primaryStorage;
    }

    [[nodiscard]] bool MonsterManagementViewModel::swapMonsters(
        MonPosition lhs, MonPosition rhs) {
        Expects(lhs.positions.size() == rhs.positions.size());
        if (std::tie(lhs.storage, lhs.box) == std::tie(rhs.storage, rhs.box)) {
            sol::table storage = getStorage(lhs.storage).getStorage();
            sol::protected_function swap = _monswap["swapSameBox"];
            auto storageOpt = mapResult<sol::table>(swap(storage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                sol::as_table(rhs.positions)));
            if (storageOpt) {
                getStorage(lhs.storage).setStorage(*storageOpt);
                return true;
            } else {
                return false;
            }

        } else if (lhs.storage == rhs.storage) {
            sol::table storage = getStorage(lhs.storage).getStorage();
            sol::protected_function swap = _monswap["swapSameStorage"];
            auto storageOpt = mapResult<sol::table>(swap(storage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                rhs.box + 1,
                sol::as_table(rhs.positions)));
            if (storageOpt) {
                getStorage(lhs.storage).setStorage(*storageOpt);
                return true;
            } else {
                return false;
            }

        } else {
            sol::table lStorage = getStorage(lhs.storage).getStorage();
            sol::table rStorage = getStorage(rhs.storage).getStorage();
            sol::protected_function swap = _monswap["swap"];
            sol::protected_function_result result = swap(lStorage,
                lhs.box + 1,
                sol::as_table(lhs.positions),
                rStorage,
                rhs.box + 1,
                sol::as_table(rhs.positions));
            auto storageOpt = mapResult<std::tuple<sol::table, sol::table>>(result);
            if (storageOpt) {
                auto& [newL, newR] = *storageOpt;
                getStorage(lhs.storage).setStorage(newL);
                getStorage(rhs.storage).setStorage(newR);
                return true;
            } else {
                return false;
            }
        }
    }

    MonsterStorageViewModel& MonsterManagementViewModel::getStorage(Storage s) {
        switch (s) {
        case Storage::Primary: return _primaryStorage;
        default: Expects(false);
        }
    }

    std::array<sol::table, 1>
    MonsterManagementViewModel::getMonsterStorages() const {
        return {_primaryStorage.getStorage()};
    }

    void MonsterManagementViewModel::selectMonsters(vm::MonPosition newSelection) {
        if (_currentSelection) {
            MonPosition oldSelection = {_currentSelection->storage,
                _currentSelection->box,
                _currentSelection->positions};
            auto swapped = swapMonsters(oldSelection, newSelection);
            if (auto handler = _handler.lock(); handler && swapped) {
                handler->onSwapped(oldSelection, newSelection);
            }
            _currentSelection = std::nullopt;
        } else {
            auto positions =
                Monlib::Util::make_dynarray<int>(newSelection.positions.size());
            using std::begin, std::end;
            std::copy(begin(newSelection.positions),
                end(newSelection.positions),
                begin(positions));
            _currentSelection = OwningMonPosition{
                newSelection.storage,
                newSelection.box,
                std::move(positions),
            };
        }
    }

    void MonsterManagementViewModel::setHandler(
        std::weak_ptr<MonsterManagementEventHandler> handler) {
        _handler = std::move(handler);
    }

    std::shared_ptr<MonsterManagementViewModel> MonsterManagementFactoryImpl::
    operator()(std::shared_ptr<sol::state> state, sol::table primaryStorage) {
        return std::make_shared<MonsterManagementViewModel>(std::move(state),
            std::move(primaryStorage),
            _monswap,
            _monsterDetailFactories);
    }

    void MonsterManagementFactoryImpl::registerImplicitMigration(
        std::string_view from,
        std::string_view to,
        sol::function convertTo,
        sol::function convertBack) {
        sol::function addMigration = _monswap["addMigration"];
        addMigration(from, to, convertTo, convertBack);
    }

    MonsterManagementFactoryImpl::MonsterManagementFactoryImpl(sol::state_view lua,
        const char* filename,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _monswap{lua.script_file(filename).get<sol::table>()},
        _monsterDetailFactories{monsterDetailFactories} {
        Ensures(_monswap && _monsterDetailFactories);
    }
}
