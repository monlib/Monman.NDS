#include "usugatapc/vm/MonsterStorageViewModel.h"

#include "usugatapc/vm/SafeLua.h"

namespace usugatapc::vm {
    namespace {
        int WrapAround(int upperBound, int index) {
            auto saneIndex = index - 1;
            auto saneResult = (saneIndex % upperBound + upperBound) % upperBound;
            auto stupidFinalResult = saneResult + 1;
            return stupidFinalResult;
        }
    }

    MonsterStorageViewModel::MonsterStorageViewModel(std::shared_ptr<sol::state> lua,
        sol::table storage,
        SharedMonsterDetailFactories monsterDetailFactories) :
        _handler{},
        _lua{std::move(lua)},
        _monsterDetailFactories{std::move(monsterDetailFactories)},
        _storage{std::move(storage)},
        _currentIndex{1} {
        Expects(_lua && _storage);
        auto length = _storage.size();  // Probably crashes if __len fails
        auto boxOpt = [&]() -> std::optional<sol::table> {
            if (length >= 1) {
                return mapResult<sol::table>(luaSafeGet(*_lua, _storage, 1));
            } else {
                return std::nullopt;
            }
        }();
        if (boxOpt.has_value()) {
            _currentBox = std::move(*boxOpt);
        } else {
            throw std::runtime_error("Could not open first box in monster storage!");
        }
    }

    void MonsterStorageViewModel::setEventHandler(
        std::weak_ptr<vm::MonsterStorageEventHandler> handler) {
        _handler = std::move(handler);
    }

    void MonsterStorageViewModel::changeBoxBy(int change) {
        auto storageLength = gsl::narrow<int>(_storage.size());
        if (storageLength < 1) {
            throw std::runtime_error("Monster storage is empty!");
        }
        auto newIndex = WrapAround(storageLength, _currentIndex + change);
        auto newBox = mapResult<sol::table>(luaSafeGet(*_lua, _storage, newIndex));
        if (!newBox.has_value()) {
            throw std::runtime_error("Could not load box!");
        }
        _currentIndex = newIndex;
        _currentBox = newBox.value();
        if (auto handler = _handler.lock()) {
            handler->onBoxChanged(change);
        }
    }

    std::optional<std::string> MonsterStorageViewModel::getCurrentBoxName() {
        Expects(_lua);
        auto lookupResult = luaSafeGet(*_lua, _currentBox, "name");
        return mapResult<std::string>(std::move(lookupResult));
    }

    std::optional<BriefMonInfo> MonsterStorageViewModel::getBriefInfo(int index) {
        Expects(_lua && _currentBox);
        auto monOpt =
            mapResult<sol::table>(luaSafeGet(*_lua, _currentBox, index + 1));
        if (!monOpt.has_value()) {
            return std::nullopt;
        }
        auto& mon = monOpt.value();
        bool isEgg =
            mapResult<bool>(luaSafeGet(*_lua, mon, "isEgg")).value_or(false);
        if (isEgg) {
            return BriefMonInfo{0, 0, isEgg};
        } else {
            return BriefMonInfo{
                mapResult<int>(luaSafeGet(*_lua, mon, "species")).value_or(0),
                mapResult<int>(luaSafeGet(*_lua, mon, "form")).value_or(0),
                isEgg,
            };
        }
    }

    std::optional<std::unique_ptr<MonsterDetailViewModel>>
    MonsterStorageViewModel::getDetailedInfo(int index) {
        auto monOpt =
            mapResult<sol::table>(luaSafeGet(*_lua, _currentBox, index + 1));
        if (!monOpt.has_value()) {
            return std::nullopt;
        }
        auto& mon = *monOpt;
        auto binaryFormatsOpt =
            mapResult<sol::table>(luaSafeGet(*_lua, mon, "binaryFormats"));
        if (!binaryFormatsOpt) {
            return nullptr;
        }
        auto& binaryFormats = *binaryFormatsOpt;
        for (int i = 0; i < gsl::narrow<int>(binaryFormats.size()); ++i) {
            auto formatOpt =
                mapResult<std::string>(luaSafeGet(*_lua, binaryFormats, i + 1));
            if (formatOpt) {
                auto factoryIt = _monsterDetailFactories->find(*formatOpt);
                if (factoryIt != _monsterDetailFactories->end()) {
                    return (*factoryIt).second(mon);
                }
            }
        }
        return nullptr;
    }

    int MonsterStorageViewModel::getCapacity() {
        Expects(_lua && _currentBox);
        constexpr auto unreasonablyLargeCapacity = 5'000'000;
        return mapResult<int>(luaSafeGet(*_lua, _currentBox, "capacity"))
            .value_or(unreasonablyLargeCapacity);
    }

    sol::table MonsterStorageViewModel::getStorage() const { return _storage; }
    void MonsterStorageViewModel::setStorage(sol::table storage) {
        auto newBox =
            mapResult<sol::table>(luaSafeGet(*_lua, storage, _currentIndex));
        if (!newBox.has_value()) {
            throw std::runtime_error("Could not load box!");
        }
        _storage = storage;
        _currentBox = *newBox;
    }

    int MonsterStorageViewModel::getBoxIndex() const { return _currentIndex - 1; }
}
