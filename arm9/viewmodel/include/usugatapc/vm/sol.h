#ifndef USUGATAPC_VM_SOL_H
#define USUGATAPC_VM_SOL_H

// Header for disabling warnings.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#include "sol.hpp"
#pragma GCC diagnostic pop

namespace usugatapc::vm {
    inline void openAllLuaLibraries(sol::state& s) {
        s.open_libraries(sol::lib::base,
            sol::lib::package,
            sol::lib::coroutine,
            sol::lib::string,
            sol::lib::os,
            sol::lib::math,
            sol::lib::table,
            sol::lib::debug,
            sol::lib::bit32,
            sol::lib::io);
    }
}
#endif
