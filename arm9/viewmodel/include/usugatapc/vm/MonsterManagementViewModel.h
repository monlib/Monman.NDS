#ifndef USUGATAPC_VM_MONSTERMANAGEMENTVIEWMODEL_H
#define USUGATAPC_VM_MONSTERMANAGEMENTVIEWMODEL_H

#include <memory>

#include <gsl/gsl>

#include "Monlib/Util/Dynarray.h"

#include "usugatapc/vm/MonsterStorageViewModel.h"
#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    /// Distinguishes between a MonsterManagementViewModel's storage objects
    enum class Storage { Primary };

    /// Describes a monsters position within a group of storage objects
    struct MonPosition {
        Storage storage;
        int box;
        gsl::span<int> positions;
    };

    struct MonsterManagementEventHandler {
        virtual void onSwapped(MonPosition, MonPosition) = 0;
        virtual ~MonsterManagementEventHandler() = default;
    };

    /// Manages monster storage
    class MonsterManagementViewModel {
    public:
        MonsterManagementViewModel(std::shared_ptr<sol::state>,
            sol::table primaryStorage,
            sol::table monswap,
            SharedMonsterDetailFactories);
        MonsterManagementViewModel(const MonsterManagementViewModel&) = delete;
        MonsterManagementViewModel(MonsterManagementViewModel&&) = default;
        MonsterManagementViewModel& operator=(
            const MonsterManagementViewModel&) = delete;
        MonsterManagementViewModel& operator=(
            MonsterManagementViewModel&&) = default;

        void setHandler(std::weak_ptr<MonsterManagementEventHandler>);

        /// Returns a view model for the primary monster storage system.
        gsl::not_null<MonsterStorageViewModel*> getPrimaryStorage();

        void selectMonsters(MonPosition);

        std::array<sol::table, 1> getMonsterStorages() const;

    private:
        struct OwningMonPosition {
            Storage storage;
            int box;
            Monlib::Util::dynarray<int> positions;
        };

        std::shared_ptr<sol::state> _lua;
        MonsterStorageViewModel _primaryStorage;
        SharedMonsterDetailFactories _monsterDetailFactories;
        sol::table _monswap;
        std::optional<OwningMonPosition> _currentSelection;
        std::weak_ptr<MonsterManagementEventHandler> _handler;

        MonsterStorageViewModel& getStorage(Storage);

        // Attempts to swap the monsters at the given positions. Returns true if
        // successful. Both arguments must have the same number of positions.
        [[nodiscard]] bool swapMonsters(MonPosition, MonPosition);
    };

    class MonsterManagementFactoryImpl {
    public:
        MonsterManagementFactoryImpl(
            sol::state_view, const char*, SharedMonsterDetailFactories);
        std::shared_ptr<MonsterManagementViewModel> operator()(
            std::shared_ptr<sol::state> state, sol::table primaryStorage);
        void registerImplicitMigration(
            std::string_view, std::string_view, sol::function, sol::function);

    private:
        sol::table _monswap;
        SharedMonsterDetailFactories _monsterDetailFactories;
    };

    using MonsterManagementFactory = std::shared_ptr<MonsterManagementFactoryImpl>;
}

#endif
