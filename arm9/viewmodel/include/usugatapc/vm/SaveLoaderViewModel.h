#ifndef USUGATAPC_VM_SAVELOADERVIEWMODEL_H
#define USUGATAPC_VM_SAVELOADERVIEWMODEL_H

#include <string>

#include "usugatapc/vm/LoadedSave.h"
#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    /// Loads Save objects
    class SaveLoaderViewModel {
    public:
        SaveLoaderViewModel(
            std::shared_ptr<sol::state> lua, sol::protected_function loader);

        /// Opens the save file at the given path
        LoadedSave openSave(const char*);

    private:
        std::shared_ptr<sol::state> _lua;
        sol::protected_function _loader;
    };
}

#endif
