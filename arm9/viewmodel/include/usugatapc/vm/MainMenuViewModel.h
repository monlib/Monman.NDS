#ifndef USUGATAPC_VM_MAINMENUVIEWMODEL_H
#define USUGATAPC_VM_MAINMENUVIEWMODEL_H

#include <optional>

#include "usugatapc/vm/LoadedSave.h"
#include "usugatapc/vm/MonsterManagementViewModel.h"

namespace usugatapc::vm {
    /// Provides a main menu
    class MainMenuViewModel {
    public:
        MainMenuViewModel(
            std::shared_ptr<sol::state>, sol::table, MonsterManagementFactory);
        MainMenuViewModel(const MainMenuViewModel&) = delete;
        MainMenuViewModel(MainMenuViewModel&&) = default;
        MainMenuViewModel& operator=(const MainMenuViewModel&) = delete;
        MainMenuViewModel& operator=(MainMenuViewModel&&) = default;

        /// Produces the view model for editing monsters. May be nullptr.
        std::shared_ptr<MonsterManagementViewModel> openMonsterManager();

        /// Applies the changes made to the saves' monsterStorages.
        void applyMonsterStorageChanges(const MonsterManagementViewModel&);

    private:
        std::shared_ptr<sol::state> _lua;
        sol::table _save;
        MonsterManagementFactory _makeMonsterManagement;
    };
}

#endif
