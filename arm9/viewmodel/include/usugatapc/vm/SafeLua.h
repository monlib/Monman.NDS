#ifndef USUGATAPC_VM_SAFELUA_H
#define USUGATAPC_VM_SAFELUA_H

#include <gsl/gsl>

#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    template <class T>
    inline sol::protected_function_result luaSafeGet(
        sol::state_view lua, sol::table& table, T&& key) {
        Expects(lua);
        sol::protected_function get = lua.safe_script(
            R"(return function (table, key) return table[key] end)");
        return get(table, std::forward<T>(key));
    }

    template <class T>
    inline std::optional<T> mapResult(sol::protected_function_result result) {
        if (!result.valid()) {
            return std::nullopt;
        }
        sol::object resultObj = result.get<sol::object>();
        if constexpr (std::is_same_v<T, sol::object>) {
            return resultObj;
        } else {
            if (resultObj.is<T>()) {
                return resultObj.as<T>();
            } else {
                return std::nullopt;
            }
        }
    }
}

#endif
