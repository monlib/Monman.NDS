#ifndef USUGATAPC_VM_MONSTERSTORAGEVIEWMODEL_H
#define USUGATAPC_VM_MONSTERSTORAGEVIEWMODEL_H

#include <functional>
#include <map>
#include <optional>
#include <string>

#include "usugatapc/vm/MonsterDetailViewModel.h"
#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    /// Provides summarized info for a single monster.
    struct BriefMonInfo {
        int species;
        int form;
        bool isEgg;
    };

    struct MonsterStorageEventHandler {
        virtual void onBoxChanged(int) = 0;
        virtual ~MonsterStorageEventHandler() = default;
    };

    /// Provides access to a set of boxes, one at a time.
    class MonsterStorageViewModel {
    public:
        MonsterStorageViewModel(
            std::shared_ptr<sol::state>, sol::table, SharedMonsterDetailFactories);

        /// Set the object to handle events
        void setEventHandler(std::weak_ptr<MonsterStorageEventHandler>);

        /// Change the current box by the specified number (Wraps around).
        void changeBoxBy(int);

        /// Returns the current box's name if present.
        std::optional<std::string> getCurrentBoxName();

        /// Returns summarized information for the given monster.
        std::optional<BriefMonInfo> getBriefInfo(int index);

        /// Returns a MonsterDetailViewModel implementation if the monster has a
        /// fitting binaryFormat. Returns nullopt for empty positions, nullptr for
        /// no known MonsterDetailViewModel and a pointer to a derived class
        /// otherwise.
        std::optional<std::unique_ptr<MonsterDetailViewModel>> getDetailedInfo(
            int index);

        /// Returns the capacity of the current box.
        int getCapacity();

        /// Returns the index of the current box.
        int getBoxIndex() const;

        sol::table getStorage() const;
        void setStorage(sol::table);  // Does not raise OnBoxChanged

    private:
        std::weak_ptr<MonsterStorageEventHandler> _handler;
        std::shared_ptr<sol::state> _lua;
        SharedMonsterDetailFactories _monsterDetailFactories;
        sol::table _storage;
        sol::table _currentBox;
        int _currentIndex;
    };
}

#endif
