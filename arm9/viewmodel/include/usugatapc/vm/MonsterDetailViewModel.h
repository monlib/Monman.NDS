#ifndef USUGATAPC_VM_MONSTERDETAILVIEWMODEL_H
#define USUGATAPC_VM_MONSTERDETAILVIEWMODEL_H

#include <functional>
#include <map>
#include <memory>

#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    /// Virtual base class for ViewModels providing access to a monsters properties.
    struct MonsterDetailViewModel {
        virtual ~MonsterDetailViewModel() = default;
    };

    using MonsterDetailFactory =
        std::function<std::unique_ptr<MonsterDetailViewModel>(sol::table)>;

    using MonsterDetailFactories = std::map<std::string, MonsterDetailFactory>;
    using SharedMonsterDetailFactories = std::shared_ptr<MonsterDetailFactories>;
}

#endif
