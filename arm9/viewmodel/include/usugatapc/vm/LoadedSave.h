#ifndef USUGATAPC_VM_LOADEDSAVE_H
#define USUGATAPC_VM_LOADEDSAVE_H

#include "usugatapc/vm/sol.h"

namespace usugatapc::vm {
    struct LoadedSave {
        sol::table obj;
    };
}

#endif
