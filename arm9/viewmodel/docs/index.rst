Welcome to usugatapc::vm's documentation!
=========================================

.. doxygenclass:: usugatapc::vm::Application
    :members:

.. doxygenclass:: usugatapc::vm::SaveLoaderViewModel
    :members:

.. doxygenclass:: usugatapc::vm::MainMenuViewModel
    :members:

.. doxygenenum:: usugatapc::vm::Storage

.. doxygenstruct:: usugatapc::vm::MonPosition
    :members:
    :undoc-members:

.. doxygenclass:: usugatapc::vm::MonsterManagementViewModel
    :members:

.. doxygenclass:: usugatapc::vm::MonsterStorageViewModel
    :members:

.. doxygenstruct:: usugatapc::vm::BriefMonInfo
    :members:
    :undoc-members:

.. doxygenstruct:: usugatapc::vm::MonsterDetailViewModel

.. doxygentypedef:: usugatapc::vm::MonsterDetailFactory

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
