local monswap = {}

-- migrations :: BinaryFormatTag .. BinaryFormatTag -> Monster -> Maybe Monster
local migrations = {}

-- forward decls
local swapOneInBox
local migrateMon
local boxWithMigratedMon

-- migrateTo, migrateBack :: Monster -> Maybe Monster
function monswap.addMigration(from, to, migrateTo, migrateBack)
    migrations[from .. to] = migrateTo
    migrations[to .. from] = migrateBack
end

function monswap.swapSameBox(storage, boxIndex, lIndexes, rIndexes)
    local box = storage[boxIndex]
    for i = 1, #lIndexes do
        if box == nil then return nil end
        local lIndex, rIndex = lIndexes[i] + 1, rIndexes[i] + 1
        box = swapOneInBox(box, lIndex, rIndex)
    end
    return storage:withBoxAt(box, boxIndex)
end

swapOneInBox = function (box, lIndex, rIndex)
    local newBox = boxWithMigratedMon(box, box[lIndex], rIndex)
    if not newBox then return nil end
    return boxWithMigratedMon(newBox, box[rIndex], lIndex)
end

function monswap.swapSameStorage(storage, lBoxIndex, lIndexes, rBoxIndex, rIndexes)
    assert(lBoxIndex ~= rBoxIndex)
    local lBox, rBox = storage[lBoxIndex], storage[rBoxIndex]
    for i = 1, #lIndexes do
        local lIndex, rIndex = lIndexes[i] + 1, rIndexes[i] + 1
        if not lBox or not rBox then return nil end
        lBox, rBox =
            boxWithMigratedMon(lBox, rBox[rIndex], lIndex),
            boxWithMigratedMon(rBox, lBox[lIndex], rIndex)
    end
    local newStorage = storage:withBoxAt(lBox, lBoxIndex)
    if not newStorage then return nil end
    return newStorage:withBoxAt(rBox, rBoxIndex)
end

function monswap.swap(lStorage, lBoxIndex, lIndexes, rStorage, rBoxIndex, rIndexes)
    assert(lStorage ~= rStorage)
    local lBox, rBox = lStorage[lBoxIndex], rStorage[rBoxIndex]
    for i = 1, #lIndexes do
        local lIndex, rIndex = lIndexes[i] + 1, rIndexes[i] + 1
        if not lBox or not rBox then return nil, nil end
        lBox, rBox =
            boxWithMigratedMon(lBox, rBox[rIndex], lIndex),
            boxWithMigratedMon(rBox, lBox[lIndex], rIndex)
    end
    return lStorage:withBoxAt(lBox, lBoxIndex), rStorage:withBoxAt(rBox, rBoxIndex)
end

boxWithMigratedMon = function (box, monster, index)
    local migratedMon
    if monster then -- make sure the formats match
        migratedMon = migrateMon(box, monster)
        if not migratedMon then return nil end
    end
    return box:withMonAt(migratedMon, index)
end

migrateMon = function (box, monster)
    for _, boxFormat in ipairs(box.acceptedBinaryFormats) do
        for _, monFormat in ipairs(monster.binaryFormats) do
            if boxFormat == monFormat then
                return monster
            elseif migrations[monFormat .. boxFormat] then
                local migratedMon = migrations[monFormat .. boxFormat](monster)
                if migratedMon then return monster end
            end
        end
    end
end

return monswap