import os
from conans import ConanFile, CMake

# Dependencies
lua = 'lua/5.3.4@monlib/stable'
sol2 = 'sol2/2.20.4@monlib/stable'
gsl_microsoft = 'gsl_microsoft/20180102@bincrafters/stable'
catch2 = 'catch2/2.1.0@bincrafters/stable'
ds_target = 'ds-target/0.2.0@monlib/stable'
libnds = 'libnds/v3@monlib/stable'
nitrofs = 'nitro-filesystem/v4@monlib/stable'
monlib_games_gen1 = 'Monlib.Games.Gen1/0.4.0-rc0@monlib/stable'
monlib_util = 'Monlib.Util/v2@monlib/stable'
utf8_cpp = 'UTF8-CPP/2.3.5@monlib/stable'

class UsugataPcArm9Conan(ConanFile):
    name = 'usugataPC-ARM9'
    license = 'GPLv3+'
    url = 'https://gitlab.com/monlib/Monman.NDS.git'
    description = 'ARM9 binary for usugataPC'
    settings = 'os', 'compiler', 'build_type', 'arch'
    options = {
        'withTests': [True, False, ],
        'withView': [True, False, ],
    }
    default_options = 'withTests=False', 'withView=True'
    generators = 'cmake_find_package'
    exports_sources = ('view/src/*', 'view/include/*', 'view/CMakeLists.txt',
        'viewmodel/src/*', 'viewmodel/include/*', 'viewmodel/CMakeLists.txt',
        'viewmodel/lua/*', 'test/*', 'CMakeLists.txt')


    def config_options(self):
        if self.settings.os == 'NDS':
            del self.options.withTests
        if self.settings.os != 'NDS' or self.settings.arch != 'armv5te':
            del self.options.withView

    def requirements(self):
        base_req = {lua, sol2, gsl_microsoft, monlib_util, }
        test_req = {catch2, }
        view_req = {
            ds_target,
            libnds,
            nitrofs,
            monlib_games_gen1,
            monlib_util,
            gsl_microsoft,
            utf8_cpp,
        }
        actual_req = (base_req
            | (test_req if self._shouldBuildTests() else set())
            | (view_req if self._shouldBuildView() else set()))
        for req in actual_req:
            self.requires(req, private=True)

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build(target='usugataVM')
        if self._shouldBuildView():
            cmake.build(target='usugataPC')
        if self._shouldBuildTests():
            cmake.build(target='usugataVM-tests')
            os.environ['CTEST_OUTPUT_ON_FAILURE'] = '1'
            cmake.test()

    def package(self):
        # Test binary and viewmodel library aren't exported, because there is
        # no package that needs them.
        self.copy('view/usugataPC', dst='bin', keep_path=False)
        self.copy('viewmodel/lua/*', dst='lua', keep_path=False)

    def _shouldBuildView(self):
        return 'withView' in self.options and self.options.withView == True

    def _shouldBuildTests(self):
        return 'withTests' in self.options and self.options.withTests == True
