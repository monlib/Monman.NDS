#ifndef USUGATAPC_UTILITY_H
#define USUGATAPC_UTILITY_H

#include <algorithm>
#include <cstddef>
#include <cstdio>

#include <gsl/gsl>

#include "Monlib/Util/Dynarray.h"

namespace usugatapc {
    struct FileDeleter {
    public:
        void operator()(std::FILE* f) noexcept {
            // Don't check return value, because we're probably in a dtor and
            // can't throw.
            static_cast<void>(std::fclose(f));
        }
    };

    using UniqueFile = std::unique_ptr<std::FILE, FileDeleter>;

    inline void loadFile(const char* filename, void* dest) {
        Expects(filename && dest);
        UniqueFile f{std::fopen(filename, "rb")};
        if (f) {
            std::fseek(f.get(), 0, SEEK_END);
            auto len = std::ftell(f.get());
            std::fseek(f.get(), 0, SEEK_SET);
            std::fread(dest, 1, gsl::narrow<std::size_t>(len), f.get());
        } else {
            std::fprintf(stderr, "File not found: %s\n", filename);
            throw std::runtime_error("Failed to open file!");
        }
    }

    template <class T>
    inline Monlib::Util::shared_dynarray<T> loadFileShared(const char* filename) {
        Expects(filename);
        UniqueFile f{std::fopen(filename, "rb")};
        if (f) {
            std::fseek(f.get(), 0, SEEK_END);
            auto len = std::ftell(f.get());
            std::fseek(f.get(), 0, SEEK_SET);
            auto data = Monlib::Util::make_shared_dynarray<T>(gsl::narrow<int>(len));
            std::fread(data.data(), 1, gsl::narrow<std::size_t>(len), f.get());
            return data;
        } else {
            std::fprintf(stderr, "File not found: %s\n", filename);
            throw std::runtime_error("Failed to open file!");
        }
    }

    template <class U, class T>
    auto positiveModulo(U x, T n) {
        return (x % n + n) % n;
    }

    // Helper struct for creating begin/end pairs.
    template <class TBegin, class TEnd = TBegin>
    struct EasyRange {
        TBegin beginIt;
        TEnd endIt;
        TBegin begin() { return beginIt; }
        TBegin end() { return endIt; }
    };
    template <class T>
    EasyRange(T&& b, T&& e)->EasyRange<std::remove_cv_t<std::remove_reference_t<T>>>;

    template <class T>
    struct TemporaryArrowValue {
        T elem;
        T* operator->() const { return &elem; }
    };

    // Iterator for iterating 2D rectangles
    class RectangleIterator {
    public:
        using self_type = RectangleIterator;
        using value_type = std::tuple<int, int>;
        using reference = value_type&;
        using pointer = value_type*;
        using iterator_category = std::input_iterator_tag;

        constexpr RectangleIterator(int currX, int currY, int minX, int maxX) :
            _currX{currX},
            _currY{currY},
            _minX{minX},
            _maxX{maxX} {
            Expects(_minX <= _currX && _currX <= _maxX);
        }

        constexpr bool operator==(const RectangleIterator& other) const {
            Expects(std::tie(_minX, _maxX) == std::tie(other._minX, other._maxX));
            return std::tie(_currX, _currY) == std::tie(other._currX, other._currY);
        }
        constexpr bool operator!=(const RectangleIterator& other) const {
            return !(*this == other);
        }
        constexpr auto operator-> () const {
            return TemporaryArrowValue<value_type>{operator*()};
        }
        constexpr value_type operator*() const { return {_currX, _currY}; }
        constexpr self_type& operator++() {
            _currX++;
            if (_currX == _maxX) {
                _currX = _minX;
                ++_currY;
            }
            return *this;
        }
        constexpr self_type operator++(int) {
            self_type cpy = *this;
            operator++();
            return cpy;
        }

    private:
        int _currX;
        int _currY;
        int _minX;
        int _maxX;
    };

    inline EasyRange<RectangleIterator> iterateRect(
        int minX, int minY, int maxX, int maxY) {
        return {{minX, minY, minX, maxX}, {minX, maxY, minX, maxX}};
    }
    inline EasyRange<RectangleIterator> iterateRect(int maxX, int maxY) {
        return iterateRect(0, 0, maxX, maxY);
    }

    // standard algorithms, but for ranges
    template <class InRange, class OutRange, class F>
    auto transform(InRange&& in, OutRange&& out, F&& f) {
        using std::begin, std::end;
        return std::transform(begin(in), end(in), begin(out), std::forward<F>(f));
    }
    template <class InRange0, class InRange1, class OutRange, class F>
    auto transform(InRange0&& in0, InRange1&& in1, OutRange&& out, F&& f) {
        using std::begin, std::end;
        return std::transform(
            begin(in0), end(in0), begin(in1), begin(out), std::forward<F>(f));
    }
    template <class Range, class Integer>
    auto iota(Range&& out, Integer&& i) {
        using std::begin, std::end;
        return std::iota(begin(out), end(out), std::forward<Integer>(i));
    }
    template <class InRange, class OutRange>
    auto copy(InRange&& in, OutRange&& out) {
        using std::begin, std::end;
        return std::copy(begin(in), end(in), begin(out));
    }
    template <class Range, class T>
    auto fill(Range&& out, T&& obj) {
        using std::begin, std::end;
        return std::fill(begin(out), end(out), std::forward<T>(obj));
    }

    // gsl helper functions
    template <class... Args>
    gsl::not_null<Args...> make_not_null(Args&&... args) {
        return gsl::not_null<Args...>{std::forward<Args>(args)...};
    }

    // Typedefs for expressiveness

    template <class... Args>
    using delayed_init = std::optional<Args...>;
    using initialized_later_t = std::nullopt_t;
    constexpr const inline initialized_later_t& initializedLater = std::nullopt;

    // std::visit helper
    template <class... Ts>
    struct overloaded : Ts... {
        using Ts::operator()...;
    };
    template <class... Ts>
    overloaded(Ts...)->overloaded<Ts...>;

    // std::byte equivalents (useful for VRAM bytes)
    // Uses lowercase to match std::byte.
    enum class halfword : uint16_t {};
    enum class fullword : uint32_t {};

    template <class T, class F>
    constexpr T enumBinOp(T l, T r, F f) {
        using U = std::underlying_type_t<T>;
        return static_cast<T>(f(static_cast<U>(l), static_cast<U>(r)));
    }
    template <class T, class F>
    constexpr T& mutEnumBinOp(T& l, T r, F f) {
        using U = std::underlying_type_t<T>;
        return l = static_cast<T>(f(static_cast<U>(l), static_cast<U>(r)));
    }

    constexpr inline halfword operator|(halfword l, halfword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a | b; });
    }
    constexpr inline halfword operator&(halfword l, halfword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a & b; });
    }
    constexpr inline halfword operator^(halfword l, halfword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a ^ b; });
    }
    constexpr inline halfword& operator|=(halfword& l, halfword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a | b; });
    }
    constexpr inline halfword& operator&=(halfword& l, halfword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a & b; });
    }
    constexpr inline halfword& operator^=(halfword& l, halfword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a ^ b; });
    }
    constexpr inline halfword operator~(halfword o) {
        return static_cast<halfword>(~static_cast<uint16_t>(o));
    }
    template <class T>
    constexpr halfword operator<<(halfword l, T r) {
        return static_cast<halfword>(static_cast<uint16_t>(l) << r);
    }
    template <class T>
    constexpr halfword operator>>(halfword l, T r) {
        return static_cast<halfword>(static_cast<uint16_t>(l) >> r);
    }
    template <class T>
    constexpr halfword& operator<<=(halfword& l, T r) {
        return l = static_cast<halfword>(static_cast<uint16_t>(l) << r);
    }
    template <class T>
    constexpr halfword& operator>>=(halfword& l, T r) {
        return l = static_cast<halfword>(static_cast<uint16_t>(l) >> r);
    }
    constexpr inline fullword operator|(fullword l, fullword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a | b; });
    }
    constexpr inline fullword operator&(fullword l, fullword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a & b; });
    }
    constexpr inline fullword operator^(fullword l, fullword r) {
        return enumBinOp(l, r, [](auto a, auto b) { return a ^ b; });
    }
    constexpr inline fullword& operator|=(fullword& l, fullword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a | b; });
    }
    constexpr inline fullword& operator&=(fullword& l, fullword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a & b; });
    }
    constexpr inline fullword& operator^=(fullword& l, fullword r) {
        return mutEnumBinOp(l, r, [](auto a, auto b) { return a ^ b; });
    }
    constexpr inline fullword operator~(fullword o) {
        return static_cast<fullword>(~static_cast<uint32_t>(o));
    }
    template <class T>
    constexpr fullword operator<<(fullword l, T r) {
        return static_cast<fullword>(static_cast<uint32_t>(l) << r);
    }
    template <class T>
    constexpr fullword operator>>(fullword l, T r) {
        return static_cast<fullword>(static_cast<uint32_t>(l) >> r);
    }
    template <class T>
    constexpr fullword& operator<<=(fullword& l, T r) {
        return l = static_cast<fullword>(static_cast<uint32_t>(l) << r);
    }
    template <class T>
    constexpr fullword& operator>>=(fullword& l, T r) {
        return l = static_cast<fullword>(static_cast<uint32_t>(l) >> r);
    }

    template <class T, unsigned offset, unsigned size, class U>
    struct BitfieldVal {
        static constexpr U bitmask{(1 << size) - 1};
        using this_t = BitfieldVal;

        U& d;

        template <class U_ = U>
        constexpr std::enable_if_t<!std::is_const_v<U_>, this_t&> operator=(
            const T& rhs) {
            d = (d & ~(bitmask << offset))
                | ((static_cast<U>(rhs) & bitmask) << offset);
            return *this;
        }
        constexpr operator T() const {
            return static_cast<T>((d >> offset) & bitmask);
        }

        template <class U_ = U>
        constexpr std::enable_if_t<!std::is_const_v<U_>, this_t&> operator+=(
            const T& rhs) {
            return *this = static_cast<T>(*this) + rhs;
        }
    };
    template <class T, unsigned offset, unsigned size, class U>
    constexpr auto makeBitfieldVal(U& data) {
        return BitfieldVal<T, offset, size, U>{data};
    }
}

#endif
