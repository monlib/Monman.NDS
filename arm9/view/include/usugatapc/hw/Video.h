#ifndef USUGATAPC_HW_VIDEO_H
#define USUGATAPC_HW_VIDEO_H

#include <climits>

#include "usugatapc/Utility.h"

namespace usugatapc::hw {
    struct Tile4 {
        fullword data[8];

        static constexpr auto Bpp = 4;

        static Tile4 solidColor(int col) {
            fullword f{0};
            for (int i = 0; i < 8; ++i) {
                f = fullword(static_cast<uint32_t>(f) | col << i * 4);
            }
            Tile4 t{};
            using std::begin, std::end;
            std::fill(begin(t.data), end(t.data), f);
            return t;
        }

        void writePixel(int x, int y, int color) {
            Expects(0 <= x && x < 8 && 0 <= y && y < 8 && 0 <= color && color < 16);
            auto pixelIndex = 8 * y + x;
            auto pixelInWord = pixelIndex % (sizeof(fullword) * CHAR_BIT / Bpp);
            auto wordIndex = pixelIndex * Bpp / CHAR_BIT / sizeof(fullword);
            auto mask = ~(fullword{0xF} << (pixelInWord * Bpp));
            auto& word = data[wordIndex];
            word = (word & mask)
                | (static_cast<fullword>(color) << (pixelInWord * Bpp));
        }
    };
    static_assert(sizeof(Tile4) == 0x20);

    // Replaces libnds' TileMapEntry16, which is underaligned and not VRAM-safe.
    struct TileMapEntry16 {
        halfword data;

        constexpr TileMapEntry16(int tile, bool hflip, bool vflip, int subpal) :
            data{static_cast<halfword>(tile | static_cast<int>(hflip) << 10
                | static_cast<int>(vflip) << 11 | subpal << 12)} {
            Expects(0 <= tile && tile < 1024 && 0 <= subpal && subpal < 16);
        }
    };
    static_assert(sizeof(TileMapEntry16) == 2);

    enum class VideoEngine { Main, Sub };

    // Replaces libnds' oam API, which is burdened with memory management and can't
    // read all requried properties.

    // This class doesn't use bitfields because they often generate single-byte
    // writes or incorrect size/alignment and are generally a pain.
    enum class ObjectMode { Normal = 0, Affine = 1, Hidden = 2, Double = 3 };
    enum class BlendMode { Normal = 0, Blend = 1, Window = 2, Bitmap = 3 };
    enum class ColorMode { Colors16 = 0, Bpp4 = 0, Colors256 = 1, Bpp8 = 1 };
    enum class ObjectShape { Square = 0, Wide = 1, Tall = 2, Forbidden = 3 };
    enum class ObjectSize { k8 = 0, k16 = 1, k32 = 2, k64 = 3 };

    struct OamEntry {
        halfword attr[3];

        template <class T, auto offset, auto size>
        using field = BitfieldVal<T, offset, size, halfword>;

        constexpr auto y() { return field<int, 0x0, 8>{attr[0]}; }
        constexpr auto objectMode() { return field<ObjectMode, 0x8, 2>{attr[0]}; }
        constexpr auto blendMode() { return field<BlendMode, 0xA, 2>{attr[0]}; }
        constexpr auto mosaic() { return field<bool, 0xC, 1>{attr[0]}; }
        constexpr auto colorMode() { return field<ColorMode, 0xD, 1>{attr[0]}; }
        constexpr auto shape() { return field<ObjectShape, 0xE, 2>{attr[0]}; }
        constexpr auto x() { return field<int, 0x0, 9>{attr[1]}; }
        constexpr auto affineIndex() { return field<int, 0x9, 5>{attr[1]}; }
        constexpr auto hFlip() { return field<bool, 0xC, 1>{attr[1]}; }
        constexpr auto vFlip() { return field<bool, 0xD, 1>{attr[1]}; }
        constexpr auto size() { return field<ObjectSize, 0xE, 2>{attr[1]}; }
        constexpr auto sprite() { return field<int, 0x0, 10>{attr[2]}; }
        constexpr auto priority() { return field<int, 0xA, 2>{attr[2]}; }
        constexpr auto palette() { return field<int, 0xC, 4>{attr[2]}; }
    };
    static_assert(sizeof(OamEntry) == 6);

    struct OamBlock {
        OamEntry entry;
        halfword attr3;
    };
    static_assert(sizeof(OamBlock) == 8);

    // Acesses dispcnt directly.
    void initOam(VideoEngine, int spriteMapping, bool enableExtPal);

    struct OamShadow {
        std::array<OamBlock, 128> blocks;

        constexpr OamEntry& getEntry(int i) {
            Expects(0 <= i && i < gsl::narrow_cast<int>(blocks.size()));
            return blocks[i].entry;
        }

        constexpr const OamEntry& getEntry(int i) const {
            Expects(0 <= i && i < gsl::narrow_cast<int>(blocks.size()));
            return blocks[i].entry;
        }

        // Must not be called outside VBlank
        void copyToOam(VideoEngine);
    };

    // Actual OAM, placed using asm
    extern "C" OamBlock g_usugatapc_hw_mainOam[128];
    extern "C" OamBlock g_usugatapc_hw_subOam[128];

    inline constexpr OamBlock (&g_mainOam)[128] = g_usugatapc_hw_mainOam;
    inline constexpr OamBlock (&g_subOam)[128] = g_usugatapc_hw_subOam;

    enum class SpriteBoundary { k32, k64, k128, k256 };

    [[gnu::const]] inline int spriteIndex(
        SpriteBoundary boundary, const volatile void* ptr) {
        auto bytesPerTile = 1 << (static_cast<int>(boundary) + 5);
        return (reinterpret_cast<uintptr_t>(ptr) / bytesPerTile) % 1024;
    }
}

#endif
