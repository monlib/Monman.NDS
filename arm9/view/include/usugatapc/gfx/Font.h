#ifndef USUGATAPC_GFX_FONT_H
#define USUGATAPC_GFX_FONT_H

#include <cstdint>
#include <optional>

#include "Monlib/Util/Dynarray.h"

#include "usugatapc/Utility.h"
#include "utf8.h"

namespace usugatapc::gfx {

    class GlyphView {
    public:
        GlyphView(const halfword*);
        int getHeight() const;
        int getWidth() const;
        int getYOffset() const;
        int getSpacing() const;
        int getPixelCount() const;
        int getPixel(int index) const;
        int getPixel(int x, int y) const;

    private:
        const halfword* _data;
        int16_t getHeader() const;
    };

    class Font {
    public:
        explicit Font(Monlib::Util::shared_dynarray<const fullword>);
        std::optional<GlyphView> getGlyph(int codepoint) const;

    private:
        struct UnicodeGlyphMapEntry {
            int32_t codepoint;
            int32_t glyphIndex;
        };

        Monlib::Util::shared_dynarray<const fullword> _data;
        gsl::span<const UnicodeGlyphMapEntry> _conversionTable;
        gsl::span<const halfword> _glyphTable;
    };

    inline EasyRange<utf8::iterator<std::string_view::iterator>> utf8Range(
        std::string_view text) {
        using std::begin, std::end;
        auto textBegin = begin(text);
        auto textEnd = end(text);
        utf8::iterator rangeBegin{textBegin, textBegin, textEnd};
        utf8::iterator rangeEnd{textEnd, textBegin, textEnd};
        return EasyRange{rangeBegin, rangeEnd};
    }

    template <class F>
    void drawTextPerPixel(const Font& font, std::string_view text, F&& drawPx) {
        const auto& fallbackGlyph = font.getGlyph(0xFFFD).value();
        int glyphX = 0;
        for (auto codepoint : utf8Range(text)) {
            const auto& glyph = font.getGlyph(codepoint).value_or(fallbackGlyph);
            if (glyphX != 0) {
                glyphX += glyph.getSpacing();
            }
            for (auto [x, y] : iterateRect(glyph.getWidth(), glyph.getHeight())) {
                auto col = glyph.getPixel(x, y);
                drawPx(col, x + glyphX + glyph.getSpacing(), y + glyph.getYOffset());
            }
            glyphX += glyph.getWidth();
        }
    }
}

#endif
