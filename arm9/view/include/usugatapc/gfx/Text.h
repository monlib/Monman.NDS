#ifndef USUGATAPC_GFX_TEXT_H
#define USUGATAPC_GFX_TEXT_H

#include "usugatapc/gfx/Font.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/hw/nds.h"

namespace usugatapc::gfx {
    struct TextAreaDescriptor {
        int height, width, mapX, mapY, textX, textY, firstTile;
    };

    class TextArea {
    public:
        TextArea(TextAreaDescriptor desc, int bg, int subpal);
        TextArea(TextAreaDescriptor desc,
            gsl::span<hw::TileMapEntry16> map,
            gsl::span<hw::Tile4> gfx,
            int subpal);
        void clear();
        void write(std::string_view text,
            const Font& font,
            std::array<std::optional<int>, 4> pal);
        static void draw(int baseX,
            int baseY,
            std::string_view text,
            const Font& font,
            int areaHeight,
            gsl::span<hw::Tile4> tiles,
            std::array<std::optional<int>, 4> pal);
        static void init(int baseX,
            int baseY,
            int width,
            int height,
            int firstIndex,
            int subpalette,
            gsl::span<hw::TileMapEntry16> mapBase);

    private:
        TextAreaDescriptor _desc;
        gsl::span<hw::Tile4> _gfx;
    };

    void drawText(const TextAreaDescriptor&,
        int bg,
        int subpal,
        std::string_view,
        const Font&,
        std::array<std::optional<int>, 4>);
}

#endif
