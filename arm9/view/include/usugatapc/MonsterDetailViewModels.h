#ifndef USUGATAPC_MONSTERDETAILVIEWMODELS_H
#define USUGATAPC_MONSTERDETAILVIEWMODELS_H

#include "usugatapc/vm/MonsterDetailViewModel.h"
#include "usugatapc/vm/sol.h"

namespace usugatapc {
    using vm::MonsterDetailViewModel;

    class Gen1MonsterDetailViewModel : public MonsterDetailViewModel {
    public:
        Gen1MonsterDetailViewModel(sol::table);
        virtual ~Gen1MonsterDetailViewModel() override = default;

        std::string getNickname();
        std::string getOtName();
        int getSpecies();
        int getOtId();

    private:
        sol::table _monster;
    };
}

#endif
