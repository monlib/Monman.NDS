#ifndef USUGATAPC_UI_FILESELECTVIEW_H
#define USUGATAPC_UI_FILESELECTVIEW_H

#include <functional>
#include <memory>
#include <string>

#include "usugatapc/ui/RootView.h"

namespace usugatapc::ui {
    class FileSelectView : public RootView {
    public:
        FileSelectView(std::function<std::shared_ptr<RootView>(std::string)>);
        virtual void update() override;
        ~FileSelectView();

    private:
        // pImpl to encapsulate std::filesystem usage
        struct impl;
        std::unique_ptr<impl> _impl;
    };
}

#endif
