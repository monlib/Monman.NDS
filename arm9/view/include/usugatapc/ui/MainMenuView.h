#ifndef USUGATAPC_UI_MAINMENUVIEW_H
#define USUGATAPC_UI_MAINMENUVIEW_H

#include "usugatapc/Utility.h"
#include "usugatapc/ui/RootView.h"
#include "usugatapc/vm/MainMenuViewModel.h"

namespace usugatapc::ui {
    class MainMenuView : public RootView {
    public:
        explicit MainMenuView(std::shared_ptr<vm::MainMenuViewModel>);
        virtual void update() override;

    private:
        struct Video {
            Video();
            void moveHighlightsToSelection(int selection);
        };
        delayed_init<Video> _video;
        std::shared_ptr<vm::MainMenuViewModel> _vm;
        int _currSelection;

        void moveHighlight(uint32_t keys);
        void switchToMonsterView();
        void switchToCredits();
    };
}

#endif
