#ifndef USUGATAPC_UI_INPUTMENU_H
#define USUGATAPC_UI_INPUTMENU_H

#include <variant>

#include <gsl/gsl>

#include "Monlib/Util/Dynarray.h"

namespace usugatapc::inputgraphdetail {
    using NodeId = int;
    enum class Direction { Up, Down, Left, Right };
    struct Position;

    class SimpleNode {
    public:
        struct PosDetail {
            constexpr bool operator==(const PosDetail) const { return true; }
            constexpr bool operator!=(const PosDetail) const { return false; }
        };

        SimpleNode(NodeId up, NodeId down, NodeId left, NodeId right);
        NodeId leave(NodeId, PosDetail, Direction) const;
        PosDetail enter(NodeId, Position, Direction) const;
        PosDetail start() const;

    private:
        NodeId _up, _down, _left, _right;
    };

    class RectNode {
    public:
        struct PosDetail {
            int x, y;
            bool operator==(const PosDetail& rhs) const;
            bool operator!=(const PosDetail& rhs) const;
        };
        struct VertConn {
            int up, down;
        };
        struct HorizConn {
            int left, right;
        };

        RectNode(std::vector<VertConn> vertical, std::vector<HorizConn> horizontal);
        NodeId leave(NodeId thisId, PosDetail pos, Direction d) const;
        PosDetail enter(NodeId thisId, Position prev, Direction d) const;
        PosDetail start() const;

    private:
        std::vector<VertConn> _vertical;
        std::vector<HorizConn> _horizontal;
        int height() const { return _horizontal.size(); }
        int width() const { return _vertical.size(); }
    };

    using Node = std::variant<SimpleNode, RectNode>;
    using AnyPosDetail = std::variant<SimpleNode::PosDetail, RectNode::PosDetail>;
    struct Position {
        NodeId node;
        AnyPosDetail posInfo;
        bool operator==(const Position& rhs) const;
        bool operator!=(const Position& rhs) const;
    };
    class InputGraph {
    public:
        InputGraph(std::vector<Node>);
        Position startAt(NodeId) const;
        Position moved(Position, Direction) const;

    private:
        std::vector<Node> _nodes;
        const Node& get(NodeId id) const;
    };
}

namespace usugatapc::ui {
    using InputGraph = inputgraphdetail::InputGraph;
    using InputGraphPos = inputgraphdetail::Position;
}

#endif
