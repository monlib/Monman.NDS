#ifndef USUGATAPC_UI_CREDITSVIEW_H
#define USUGATAPC_UI_CREDITSVIEW_H

#include "usugatapc/Utility.h"
#include "usugatapc/gfx/Text.h"
#include "usugatapc/ui/RootView.h"

namespace usugatapc::ui {
    class CreditsView : public RootView {
    public:
        CreditsView(gsl::not_null<const char*> creditsFile,
            std::function<std::shared_ptr<RootView>()>);
        void update() override;

    private:
        class Video {
        public:
            explicit Video(std::string credits);
            void advanceFrame();
            bool finishedScrolling() const;

        private:
            std::string _credits;
            const char* _endLastLine;
            gfx::Font _font;
            delayed_init<int> _bg;
            delayed_init<gsl::span<hw::Tile4, 1024>> _tiles;
            int _lastDrawnY;
            int _firstVisY;
            bool _allTextHasBeenDrawn;

            void ensureTextPresent();
            void clearOffScreenArea();
        };
        delayed_init<Video> _video;
        gsl::not_null<const char*> _creditsFile;
        std::function<std::shared_ptr<RootView>()> _onFinished;
    };
}

#endif
