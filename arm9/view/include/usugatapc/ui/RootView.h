#ifndef USUGATAPC_UI_ROOTVIEW_H
#define USUGATAPC_UI_ROOTVIEW_H

#include <memory>

namespace usugatapc::ui {
    struct RootView {
        virtual void update() = 0;
        virtual ~RootView() = default;
    };

    void setNextRootView(std::shared_ptr<RootView>);
}

#endif
