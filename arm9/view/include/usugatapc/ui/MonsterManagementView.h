#ifndef USUGATAPC_UI_MONSTERMANAGEMENTVIEW_H
#define USUGATAPC_UI_MONSTERMANAGEMENTVIEW_H

#include <array>
#include <variant>
#include <vector>

#include "usugatapc/gfx/Text.h"
#include "usugatapc/ui/InputMenu.h"
#include "usugatapc/ui/RootView.h"
#include "usugatapc/ui/ScrollingBoxWidget.h"
#include "usugatapc/vm/MonsterManagementViewModel.h"

namespace usugatapc::ui {
    class MonsterManagementView
        : public RootView,
          public vm::MonsterManagementEventHandler,
          public std::enable_shared_from_this<MonsterManagementView> {
    public:
        MonsterManagementView(std::shared_ptr<vm::MonsterManagementViewModel>,
            std::function<std::shared_ptr<RootView>(
                const vm::MonsterManagementViewModel&)> onFinished);
        void update() override;

    private:
        struct Video {
            Video(gsl::not_null<vm::MonsterStorageViewModel*>);
            void onActiveMonChanged(
                std::optional<std::unique_ptr<vm::MonsterDetailViewModel>>);
            void updateOnVBlank();
            void positionCursorOnBoxChangeButton(bool isLeft);
            void positionCursorOnBoxBody(int x, int y);
            void updatePostLogic();
            bool isAnimating() const;
            void onSwapped(vm::MonPosition, vm::MonPosition, int);
            void positionCursorOnBackButton();

        private:
            std::unique_ptr<hw::OamShadow> _oamMain;
            std::shared_ptr<ScrollingBoxWidget> _scrollingBox;
            struct Cursor {
                gsl::span<hw::Tile4, 1> sprite;  // Probably needs to be larger
                std::reference_wrapper<hw::OamEntry> object;
            };
            delayed_init<Cursor> _cursor;
            std::array<int, 4> _mainBgs;
            std::array<int, 4> _subBgs;
            gfx::Font _font;
            delayed_init<gfx::TextArea> _otNameArea, _nicknameArea, _speciesArea,
                _otIdArea;
        };

        std::shared_ptr<vm::MonsterManagementViewModel> _vm;
        gsl::not_null<vm::MonsterStorageViewModel*> _primaryStorageVm;
        InputGraphPos _cursorPosition;
        std::function<std::shared_ptr<RootView>(
            const vm::MonsterManagementViewModel&)>
            _onFinished;
        delayed_init<Video> _video;

        void positionCursor();
        InputGraphPos newPosition(uint32_t keysDown) const;
        void executeAction();
        void updateDetails();
        void changeBox(bool isLeft);
        void selectMonster(int index);
        std::optional<int> selectedMonsterIndex() const;
        std::optional<inputgraphdetail::RectNode::PosDetail> selectedGridPos() const;
        void onSwapped(vm::MonPosition, vm::MonPosition) override;
        void onActiveMonChanged(int index);
    };
}

#endif
