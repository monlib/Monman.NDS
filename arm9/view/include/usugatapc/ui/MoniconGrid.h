#ifndef USUGATAPC_UI_MONICONGRID_H
#define USUGATAPC_UI_MONICONGRID_H

#include <tuple>

#include <gsl/gsl>

#include "Monlib/Util/Dynarray.h"

#include "usugatapc/Utility.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/vm/MonsterStorageViewModel.h"

namespace usugatapc::ui {
    class MoniconGrid {
    public:
        MoniconGrid(gsl::not_null<vm::MonsterStorageViewModel*> viewModel,
            gsl::span<hw::OamBlock, 35> objects,
            gsl::span<hw::Tile4[4 * 4], 35> sprites,
            int priority);
        void moveBy(int x);
        void loadNewColumn(int columnInBox, int x);
        void changeUnusedColumnBy(int);
        void redraw(int index);

    private:
        struct IconIndex {
            int species;
            int form;

            bool operator<(const IconIndex& rhs) const {
                const auto& lhs = *this;
                return std::tie(lhs.species, lhs.form)
                    < std::tie(rhs.species, rhs.form);
            }

            bool operator==(const IconIndex& rhs) const {
                const auto& lhs = *this;
                return std::tie(lhs.species, lhs.form)
                    == std::tie(rhs.species, rhs.form);
            }
        };
        gsl::not_null<vm::MonsterStorageViewModel*> _viewModel;
        Monlib::Util::dynarray<IconIndex> _index;
        int _regularIconCount;
        int _totalIconCount;
        UniqueFile _graphicsFile;
        int _unusedColumn;
        gsl::span<hw::OamBlock, 35> _objects;
        gsl::span<hw::Tile4[4 * 4], 35> _sprites;

        void loadMetadata();
        std::optional<int> findSpriteIndex(std::optional<vm::BriefMonInfo>) const;
        std::optional<vm::BriefMonInfo> getBriefInfo(int i);
        void loadGraphics(int column, int row, std::optional<int> gfxId);
        void initializeObjects(int priority);
        void positionObjects();
        void positionColumn(int col, int x);
        void loadSprites();
        void loadColumnGraphics(int visualColumn, int resourceColumn);
        void loadSprite(
            hw::OamEntry& entry, hw::Tile4 (&tiles)[4 * 4], int iconIndex);
        hw::OamEntry& getObject(int x, int y);
        hw::Tile4 (&getSprite(int x, int y))[4 * 4];
    };
}

#endif
