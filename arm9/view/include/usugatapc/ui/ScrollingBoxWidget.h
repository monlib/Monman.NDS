#ifndef USUGATAPC_UI_SCROLLINGBOXWIDGET_H
#define USUGATAPC_UI_SCROLLINGBOXWIDGET_H

#include <gsl/gsl>

#include "usugatapc/Utility.h"
#include "usugatapc/gfx/Font.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/ui/MoniconGrid.h"
#include "usugatapc/vm/MonsterStorageViewModel.h"

namespace usugatapc::ui {
    class ScrollingBoxWidget : public vm::MonsterStorageEventHandler {
    public:
        enum class Direction { Left, Right };

        ScrollingBoxWidget(vm::MonsterStorageViewModel* viewModel,
            gfx::Font font,
            gsl::span<halfword, 32> subpals,
            int bg,
            hw::VideoEngine engine,
            gsl::span<hw::OamBlock, 35> iconObjects,
            gsl::span<hw::Tile4[4 * 4], 35> iconSprites,
            gsl::span<hw::OamBlock, 6> headingObjects,
            gsl::span<hw::Tile4[4 * 2], 6> headingSprites);
        void advanceScrolling();
        bool isScrolling() const;
        void redraw(int index);
        void onBoxChanged(int) override;

        ~ScrollingBoxWidget() override = default;

    private:
        vm::MonsterStorageViewModel* _viewModel;
        gfx::Font _font;

        // Background state
        gsl::span<halfword, 32> _subpals;
        int _bg;
        int _currSlot;
        int _currColumn;

        // Animation state
        int _remainingScrollFrames;
        Direction _currDirection;

        MoniconGrid _monicons;

        // Box headings
        gsl::span<hw::Tile4[4 * 2], 6> _headingSprites;
        gsl::span<hw::OamBlock, 6> _headingObjects;

        void beginScrolling(Direction);
        void swapSlot();
        void loadBox();
        void positionHeading(int x);
        void loadPalette(const char*);
        void loadTiles(const char*);
        void loadBackground();
        void updateMap();
        int calculateSubpalIndex() const;
        void moveTileColumn(Direction);
        static int alignedToDirection(int, Direction);
        void startScrollFrameCount();
        void initHeadings();
        void loadHeading();
    };
}

#endif
