#include "usugatapc/ui/FileSelectView.h"

#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include <gsl/gsl>

#include "usugatapc/hw/nds.h"

// Include std::filesystem. std::experimental::filesystem is used for
// clang-based code analysis and suggestions.
#if __has_include(<filesystem>)
#include <filesystem>
namespace {
    namespace fs = std::filesystem;
}
#elif __has_include(<experimental/filesystem>)
#warning "Using std::experiental::filesystem!"
#include <experimental/filesystem>
namespace {
    namespace fs = std::experimental::filesystem;
}
#else
#error "No std::filesystem header was found!"
#endif

namespace usugatapc::ui {
    namespace {}

    struct FileSelectView::impl {
    public:
        impl(std::function<std::shared_ptr<RootView>(std::string)> onSelected) :
            _onSelected{std::move(onSelected)} {
            Expects(_onSelected);
            changePathTo(".");
        }

        void update() {
            if (!_screenIsInitialized) {
                ::consoleDemoInit();
                ::consoleDebugInit(::DebugDevice_NOCASH);
                drawScreen();
            }

            ::scanKeys();
            auto keysDown = ::keysDown();
            if (keysDown & ::KEY_DOWN) {
                _currIndex =
                    std::min(gsl::narrow<int>(_filenames.size()), _currIndex + 1);
                drawScreen();
            } else if (keysDown & ::KEY_UP) {
                _currIndex = std::max(0, _currIndex - 1);
                drawScreen();
            } else if (keysDown & ::KEY_A) {
                auto newPath = _currPath / _filenames[_currIndex];
                if (fs::is_directory(fs::status(newPath))) {
                    changePathTo(_currPath / _filenames[_currIndex]);
                } else {
                    setNextRootView(_onSelected(newPath));
                }
                drawScreen();
            }
        }

        void drawScreen() {
            int lastIndex =
                std::min(std::max(ScreenHeight, _currIndex + ScreenHeight / 2),
                    gsl::narrow<int>(_filenames.size()));
            int firstIndex = std::max(0, lastIndex - ScreenHeight);
            std::puts("\033[2J\033[H");
            for (int i = firstIndex; i < lastIndex; ++i) {
                std::printf(
                    "%c%s\n", i == _currIndex ? '>' : ' ', _filenames[i].c_str());
            }
            _screenIsInitialized = true;
        }

        void changePathTo(fs::path newPath) {
            _currPath = newPath.lexically_normal();
            _filenames.clear();
            _filenames.push_back("..");
            for (const auto& entry : fs::directory_iterator(newPath)) {
                std::string name = entry.path().filename();
                if (fs::is_directory(entry.status())) {
                    name += "/";
                }
                _filenames.push_back(name);
            }
            using std::begin, std::end;
            std::sort(begin(_filenames), end(_filenames));
            _currIndex = 0;
        }

    private:
        bool _screenIsInitialized = false;
        std::vector<std::string> _filenames{};
        fs::path _currPath;
        int _currIndex = 0;
        std::function<std::shared_ptr<RootView>(std::string)> _onSelected;
        static constexpr auto ScreenHeight = 23;
    };

    FileSelectView::FileSelectView(
        std::function<std::shared_ptr<RootView>(std::string)> onSelected) :
        _impl{std::make_unique<impl>(std::move(onSelected))} {}
    void FileSelectView::update() { return _impl->update(); }
    FileSelectView::~FileSelectView() = default;
}
