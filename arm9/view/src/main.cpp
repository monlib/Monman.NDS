#include <string_view>

#include <filesystem.h>
#include <gsl/gsl>

#include "Monlib/Games/Gen1/Lua/LuaOpen.h"

#include "usugatapc/MonsterDetailViewModels.h"
#include "usugatapc/hw/nds.h"
#include "usugatapc/ui/FileSelectView.h"
#include "usugatapc/ui/MainMenuView.h"
#include "usugatapc/ui/RootView.h"
#include "usugatapc/vm/Application.h"

extern "C" bool fatInitDefault();  // fat.h is borked

namespace usugatapc {
    namespace {
        std::shared_ptr<ui::RootView> g_rootView = nullptr;
        std::optional<std::shared_ptr<ui::RootView>> g_nextRootView = std::nullopt;

        void printFatalError(const char* error) {
            consoleDemoInit();
            ::consoleDebugInit(::DebugDevice_NOCASH);
            std::fprintf(stderr, "\nFATAL ERROR:\n%s\n", error);
            std::fputs(error, stdout);
            std::fputs("\nPress START to quit.\n", stdout);

            uint32_t keys = 0;
            while (!(keys & KEY_START)) {
                ::scanKeys();
                keys = ::keysDown();
                ::swiWaitForVBlank();
            }
        }

        bool fatWasInitializedByNitroFSAndWouldCrashIfCalledAgain() {
            // This fixes a freeze when run through DSiMenu++, where
            // nitroFSInit decides to call fatInitDefault and my code
            // then calls fatInitDefault a second time.
            // Most of the check is copied from nitroFSInit's source, except for
            // the DSi check. Without that check, desmume would fail to initialize
            // its FAT device. This freeze probably only happens because a DSi's SD
            // can't be initialized twice, but other DLDI patches might have the
            // same issue and will continue to crash.
            return ::isDSiMode() && __system_argv->argvMagic == ARGV_MAGIC
                && __system_argv->argc >= 1
                && (std::strncmp(__system_argv->argv[0], "fat", 3)
                       || std::strncmp(__system_argv->argv[0], "sd", 2));
        }

        void initialize() {
            ::consoleDebugInit(::DebugDevice_NOCASH);
            Expects(nitroFSInit(nullptr));
            if (fatWasInitializedByNitroFSAndWouldCrashIfCalledAgain()) {
                std::fprintf(stderr, "assuming nitroFSInit called fatInitDefault\n");
            } else {
                Expects(fatInitDefault());
            }

            auto app = std::make_shared<vm::Application>("nitro:/lua/monswap.lua");
            app->registerLuaModule("monlib.games.gen1", luaopen_monlib_games_gen1);
            app->registerLoaders([](auto state) -> sol::protected_function {
                return state.safe_script(R"(
                    return function(path)
                        local gen1 = require('monlib.games.gen1')
                        local mappings =  dofile('nitro:/lua/gen1mappings.lua')
                        return gen1.Save.new(path, mappings)
                    end)");
            });
            using namespace std::literals;
            app->registerMonsterDetailViewModel(
                "\x0c\x46\x62\xce\x6d\xf3\x4a\x83\x8c\x6d\xf3\x3f\xad\x06\xec\xfa"s,
                [](auto mon) {
                    return std::make_unique<Gen1MonsterDetailViewModel>(
                        std::move(mon));
                });
            app->registerMonsterDetailViewModel(
                "\xcd\x74\x31\x7b\x89\x5b\x4c\xf6\xb0\x7b\x5a\xd9\x3c\x85\x05\xe0"s,
                [](auto mon) {
                    return std::make_unique<Gen1MonsterDetailViewModel>(
                        std::move(mon));
                });

            g_rootView = std::make_shared<ui::FileSelectView>([app](auto path) {
                auto loadedSave = app->getSaveLoader().openSave(path.data());
                auto mainMenuVm = app->openMainMenu(std::move(loadedSave));
                return std::make_shared<ui::MainMenuView>(std::move(mainMenuVm));
            });
        }

        bool continueRunning() {
            Expects(g_rootView);
            g_rootView->update();
            if (g_nextRootView.has_value()) {
                g_rootView = std::move(g_nextRootView.value());
                g_nextRootView = std::nullopt;
            }
            return static_cast<bool>(g_rootView);
        }

        void mainLoop() {
            while (continueRunning()) {
                ::swiWaitForVBlank();
            }
        }

        int main() {
            try {
                initialize();
                mainLoop();
                return 0;
            } catch (const std::exception& ex) {
                printFatalError(ex.what());
                return 1;
            } catch (...) {
                printFatalError("Unknown error!");
                return 1;
            }
        }
    }

    namespace ui {
        void setNextRootView(std::shared_ptr<ui::RootView> next) {
            g_nextRootView = std::move(next);
        }
    }
}

int main() {
    return usugatapc::main();
}
