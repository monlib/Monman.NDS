#include "usugatapc/ui/CreditsView.h"

#include <utility>

#include "usugatapc/Utility.h"
#include "usugatapc/hw/nds.h"

namespace usugatapc::ui {
    namespace {
        std::string readTextFile(gsl::not_null<const char*> filename) {
            UniqueFile f{std::fopen(filename, "rb")};
            if (f) {
                std::fseek(f.get(), 0, SEEK_END);
                auto len = std::ftell(f.get());
                std::fseek(f.get(), 0, SEEK_SET);
                std::string val(gsl::narrow<std::size_t>(len), '\0');
                std::fread(val.data(),
                    sizeof(char),
                    gsl::narrow<std::size_t>(len),
                    f.get());
                return val;
            } else {
                std::fprintf(stderr, "Failed to open file!");
                return std::string{};
            }
        }

        void drawText(int baseX,
            int baseY,
            std::string_view text,
            const gfx::Font& font,
            gsl::span<hw::Tile4, 1024> tiles,
            std::array<std::optional<int>, 4> pal) {
            gfx::drawTextPerPixel(
                font, text, [&](auto fontCol, auto rawX, auto rawY) {
                    int x = (rawX + baseX) % 256;
                    int y = (rawY + baseY) % 256;
                    auto tileIndex = (y / 8) * 32 + (x / 8);
                    auto& tile = tiles[tileIndex];
                    if (auto pxCol = pal[fontCol]; pxCol) {
                        tile.writePixel(x % 8, y % 8, *pxCol);
                    }
                });
        }

        void clearArea(
            int x0, int y0, int x1, int y1, gsl::span<hw::Tile4, 1024> tiles) {
            for (auto [x, y] : iterateRect(x0, y0, x1, y1)) {
                auto trueX = (x + 256) % 256;
                auto trueY = (y + 256) % 256;
                tiles[(trueY / 8) * 32 + (trueX / 8)].writePixel(
                    trueX % 8, trueY % 8, 0);
            }
        }
        constexpr int StepSize = 1;
        constexpr int LineHeight = 14;
        constexpr std::array<std::optional<int>, 4> Pal{
            std::nullopt, 1, 2, std::nullopt};
    }

    CreditsView::CreditsView(gsl::not_null<const char*> creditsFile,
        std::function<std::shared_ptr<RootView>()> onFinished) :
        _video{initializedLater},
        _creditsFile{creditsFile},
        _onFinished{std::move(onFinished)} {
        Expects(_onFinished);
    }

    void CreditsView::update() {
        ::swiWaitForVBlank();  // reduce fps to 30 for slower scroll.
        if (_video == initializedLater) {
            _video = Video{readTextFile(_creditsFile)};
        }
        _video->advanceFrame();
        ::scanKeys();
        if (::keysDown() & ::KEY_B) {
            setNextRootView(_onFinished());
        }
    }

    CreditsView::Video::Video(std::string credits) :
        _credits{std::move(credits)},
        _endLastLine{_credits.data()},
        _font{loadFileShared<fullword>("nitro:/fonts/default.bin")},
        _bg{initializedLater},
        _tiles{initializedLater},
        _lastDrawnY{192},
        _firstVisY{0},
        _allTextHasBeenDrawn{false} {
        ::lcdMainOnTop();
        ::videoSetMode(::MODE_0_2D);
        ::vramSetPrimaryBanks(::VRAM_A_MAIN_BG,
            ::VRAM_B_MAIN_SPRITE,
            ::VRAM_C_SUB_BG,
            ::VRAM_D_SUB_SPRITE);
        _bg = ::bgInit(0, ::BgType_Text4bpp, ::BgSize_T_256x256, 0, 1);
        loadFile("nitro:/ui/mainmenu-Layer2.pal", BG_PALETTE);
        _tiles = gsl::span{reinterpret_cast<hw::Tile4*>(::bgGetGfxPtr(*_bg)), 1024};
        clearArea(0, 0, 256, 256, *_tiles);
        gsl::span map{
            reinterpret_cast<hw::TileMapEntry16*>(::bgGetMapPtr(*_bg)), 1024};
        for (int i = 0; i < map.size(); ++i) {
            map[i] = hw::TileMapEntry16{i, false, false, 0};
        }
    }

    void CreditsView::Video::advanceFrame() {
        // handle vblank stuff
        ::bgUpdate();

        // handle vdraw stuff
        if (!finishedScrolling()) {
            clearOffScreenArea();
            ensureTextPresent();
            _firstVisY += StepSize;
            ::bgSetScroll(*_bg, 0, _firstVisY);
        }
    }

    bool CreditsView::Video::finishedScrolling() const {
        return _allTextHasBeenDrawn;
    }

    void CreditsView::Video::ensureTextPresent() {
        Expects(StepSize <= LineHeight);
        bool nextLineWillBeRevealed =
            _lastDrawnY + LineHeight < _firstVisY + StepSize + 192;
        if (nextLineWillBeRevealed) {
            const char* creditsEnd = _credits.data() + _credits.size();
            if (_endLastLine != creditsEnd) {
                const char* endThisLine = std::find(_endLastLine, creditsEnd, '\n');
                std::string_view line = {_endLastLine,
                    gsl::narrow<std::size_t>(endThisLine - _endLastLine)};
                drawText(0, _lastDrawnY + LineHeight, line, _font, *_tiles, Pal);
                _endLastLine = endThisLine + 1;  // Ensure newline isn't drawn.
                _lastDrawnY += LineHeight;
            } else {
                _allTextHasBeenDrawn = true;
            }
        }
    }

    void CreditsView::Video::clearOffScreenArea() {
        clearArea(0, _firstVisY - StepSize, 256, _firstVisY, *_tiles);
    }
}
