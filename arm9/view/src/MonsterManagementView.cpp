#include "usugatapc/ui/MonsterManagementView.h"

#include "usugatapc/MonsterDetailViewModels.h"
#include "usugatapc/Utility.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/hw/nds.h"

namespace usugatapc::ui {
    namespace {
        using inputgraphdetail::SimpleNode, inputgraphdetail::RectNode;
        const InputGraph inputGraph{{
            SimpleNode{0, 2, 0, 1},
            SimpleNode{1, 2, 0, 1},
            RectNode{
                {{0, 3}, {0, 3}, {0, 3}, {1, 3}, {1, 3}, {1, 3}},
                {{0, 1}, {0, 1}, {0, 1}, {0, 1}, {0, 1}},
            },
            SimpleNode{2, 3, 2, 3},
        }};

        namespace InputNodes {
            enum {
                BoxLeft,
                BoxRight,
                BoxBody,
                BackButton,
            };
        }
    }

    MonsterManagementView::MonsterManagementView(
        std::shared_ptr<vm::MonsterManagementViewModel> vm,
        std::function<std::shared_ptr<RootView>(
            const vm::MonsterManagementViewModel&)> onFinished) :
        _vm{std::move(vm)},
        _primaryStorageVm{_vm->getPrimaryStorage()},
        _cursorPosition{inputGraph.startAt(InputNodes::BoxBody)},
        _onFinished{std::move(onFinished)},
        _video{initializedLater} {
        Expects(_onFinished != nullptr);
        // UB has already occurred at this point, this check only helps in debug.
        Expects(_vm != nullptr);
    }

    void MonsterManagementView::update() {
        if (_video == initializedLater) {
            _video.emplace(_primaryStorageVm);
            Expects(weak_from_this().use_count() > 0);
            _vm->setHandler(weak_from_this());
        }
        _video->updateOnVBlank();

        ::scanKeys();
        bool acceptsInput = !_video->isAnimating();
        if (acceptsInput) {
            auto keys = ::keysDown();
            auto newPos = newPosition(::keysDown());
            if (newPos != _cursorPosition) {
                _cursorPosition = newPos;
                updateDetails();
            }
            if (keys & ::KEY_A) {
                executeAction();
            }
        }
        _video->updatePostLogic();
        positionCursor();
    }

    void MonsterManagementView::updateDetails() {
        if (_cursorPosition.node == InputNodes::BoxBody) {
            auto index = selectedMonsterIndex();
            Expects(index);
            onActiveMonChanged(*index);
        }
    }

    void MonsterManagementView::onActiveMonChanged(int index) {
        _video->onActiveMonChanged(_primaryStorageVm->getDetailedInfo(index));
    }
    void MonsterManagementView::executeAction() {
        switch (_cursorPosition.node) {
        case InputNodes::BoxLeft: return changeBox(true);
        case InputNodes::BoxRight: return changeBox(false);
        case InputNodes::BoxBody: {
            auto selectedIndex = selectedMonsterIndex();
            Expects(selectedIndex);
            return selectMonster(*selectedIndex);
        }
        case InputNodes::BackButton: {
            setNextRootView(_onFinished(*_vm));
            break;
        }
        default: Expects(false);
        }
    }

    void MonsterManagementView::changeBox(bool isLeft) {
        _primaryStorageVm->changeBoxBy(isLeft ? -1 : +1);
    }

    void MonsterManagementView::selectMonster(int index) {
        std::vector selectedMons{index};
        vm::MonPosition rhs{
            vm::Storage::Primary,
            _primaryStorageVm->getBoxIndex(),
            selectedMons,
        };
        _vm->selectMonsters(rhs);
    }

    void MonsterManagementView::onSwapped(vm::MonPosition lhs, vm::MonPosition rhs) {
        _video->onSwapped(lhs, rhs, _primaryStorageVm->getBoxIndex());
    }

    void MonsterManagementView::positionCursor() {
        switch (_cursorPosition.node) {
        case InputNodes::BoxLeft:
            return _video->positionCursorOnBoxChangeButton(true);
        case InputNodes::BoxRight:
            return _video->positionCursorOnBoxChangeButton(false);
        case InputNodes::BoxBody: {
            auto gridPos = selectedGridPos();
            Expects(gridPos);
            return _video->positionCursorOnBoxBody(gridPos->x, gridPos->y);
        }
        case InputNodes::BackButton: {
            return _video->positionCursorOnBackButton();
        }
        default: Expects(false);
        }
    }

    InputGraphPos MonsterManagementView::newPosition(uint32_t keysDown) const {
        using inputgraphdetail::Direction;
        const auto direction = [keysDown]() -> std::optional<Direction> {
            if (keysDown & KEY_UP) {
                return Direction::Up;
            }
            if (keysDown & KEY_DOWN) {
                return Direction::Down;
            }
            if (keysDown & KEY_LEFT) {
                return Direction::Left;
            }
            if (keysDown & KEY_RIGHT) {
                return Direction::Right;
            }
            return std::nullopt;
        }();
        if (direction) {
            return inputGraph.moved(_cursorPosition, *direction);
        } else {
            return _cursorPosition;
        }
    }

    std::optional<int> MonsterManagementView::selectedMonsterIndex() const {
        auto pos = selectedGridPos();
        if (pos) {
            return pos->x + 6 * pos->y;
        } else {
            return std::nullopt;
        }
    }

    std::optional<inputgraphdetail::RectNode::PosDetail>
    MonsterManagementView::selectedGridPos() const {
        using GridPos = inputgraphdetail::RectNode::PosDetail;
        if (std::holds_alternative<GridPos>(_cursorPosition.posInfo)) {
            return std::get<GridPos>(_cursorPosition.posInfo);
        } else {
            return std::nullopt;
        }
    }

    MonsterManagementView::Video::Video(
        gsl::not_null<vm::MonsterStorageViewModel*> primaryStorageVm) :
        _oamMain{std::make_unique<hw::OamShadow>()},
        _scrollingBox{nullptr},
        _cursor{initializedLater},
        _mainBgs{},
        _subBgs{},
        _font{loadFileShared<fullword>("nitro:/fonts/default.bin")} {
        ::lcdMainOnBottom();
        ::vramSetPrimaryBanks(::VRAM_A_MAIN_BG,
            ::VRAM_B_MAIN_SPRITE,
            ::VRAM_C_SUB_BG,
            ::VRAM_D_SUB_SPRITE);

        // Bottom screen specific stuff
        ::videoSetMode(::MODE_0_2D);
        auto boxBg = ::bgInit(3, ::BgType_Text4bpp, ::BgSize_T_512x256, 0, 1);
        auto trayBg = ::bgInit(2,
            ::BgType_Text4bpp,
            ::BgSize_T_512x512,
            4,
            5);  // Leftover resources
        auto uiBg = ::bgInit(1, ::BgType_Text4bpp, ::BgSize_T_256x256, 2, 3);
        auto textBg = ::bgInit(0, ::BgType_Text4bpp, ::BgSize_T_256x256, 3, 4);
        _mainBgs = {
            textBg,
            uiBg,
            trayBg,
            boxBg,
        };
        _subBgs = {
            ::bgInitSub(0, ::BgType_Text4bpp, ::BgSize_T_256x256, 3, 4),
            ::bgInitSub(1, ::BgType_Text4bpp, ::BgSize_T_256x256, 2, 3),
            ::bgInitSub(2, ::BgType_Text4bpp, ::BgSize_T_256x256, 4, 5),
            ::bgInitSub(3, ::BgType_Text4bpp, ::BgSize_T_256x256, 0, 1),
        };

        for (int i = 0; i < 4; ++i) {
            ::bgSetPriority(_mainBgs[i], i);
            ::bgSetPriority(_subBgs[i], i);
        }

        loadFile("nitro:/ui/StorageSystem-Layer1.pal", BG_PALETTE);
        loadFile("nitro:/ui/StorageSystem-Layer1.tiles", ::bgGetGfxPtr(uiBg));
        loadFile("nitro:/ui/StorageSystem-Layer1.map", ::bgGetMapPtr(uiBg));

        loadFile("nitro:/ui/StorageSystem-Layer1.pal", BG_PALETTE_SUB);
        loadFile("nitro:/ui/StorageSystem-Layer1.tiles", ::bgGetGfxPtr(_subBgs[1]));
        loadFile("nitro:/ui/StorageSystem-Layer1.map", ::bgGetMapPtr(_subBgs[1]));

        _otNameArea = gfx::TextArea{{2, 16, 16, 4, 0, 0, 1}, _subBgs[0], 0};
        _nicknameArea = gfx::TextArea{{2, 16, 16, 8, 0, 0, 33}, _subBgs[0], 0};
        _speciesArea = gfx::TextArea{{2, 16, 16, 12, 0, 0, 65}, _subBgs[0], 0};
        _otIdArea = gfx::TextArea{{2, 16, 16, 16, 0, 0, 97}, _subBgs[0], 0};

        REG_DISPCNT &= ~DISPLAY_SPRITE_ATTR_MASK;
        REG_DISPCNT |= DISPLAY_SPR_ACTIVE | (::SpriteMapping_1D_128 & 0x0FFF'FFF0);

        _scrollingBox = std::make_shared<ScrollingBoxWidget>(primaryStorageVm,
            _font,
            gsl::span{reinterpret_cast<halfword*>(BG_PALETTE) + 224, 32},
            boxBg,
            hw::VideoEngine::Main,
            gsl::span{_oamMain->blocks.data(), 35},
            gsl::span{reinterpret_cast<hw::Tile4(*)[4 * 4]>(SPRITE_GFX), 35},
            gsl::span{_oamMain->blocks.data() + 35, 6},
            gsl::span{reinterpret_cast<hw::Tile4(*)[4 * 2]>(SPRITE_GFX) + 70, 6});
        primaryStorageVm->setEventHandler(_scrollingBox);

        auto& cursorObject = _oamMain->getEntry(35 + 6);
        auto cursorSprite = gsl::span{
            reinterpret_cast<hw::Tile4*>(SPRITE_GFX) + 35 * (4 * 4) + 6 * (4 * 2),
            1,
        };
        _cursor = Cursor{cursorSprite, cursorObject};
        cursorObject = hw::OamEntry{};
        cursorObject.shape() = hw::ObjectShape::Square;
        cursorObject.size() = hw::ObjectSize::k32;
        cursorObject.sprite() =
            spriteIndex(hw::SpriteBoundary::k128, cursorSprite.data());
        fill(cursorSprite, hw::Tile4::solidColor(1));

        Ensures(_scrollingBox != nullptr && _cursor.has_value());
    }

    void MonsterManagementView::Video::updateOnVBlank() {
        ::bgUpdate();
        _oamMain->copyToOam(hw::VideoEngine::Main);
    }

    void MonsterManagementView::Video::updatePostLogic() {
        _scrollingBox->advanceScrolling();
    }

    bool MonsterManagementView::Video::isAnimating() const {
        return _scrollingBox->isScrolling();
    }

    void MonsterManagementView::Video::onActiveMonChanged(
        std::optional<std::unique_ptr<vm::MonsterDetailViewModel>> detailedInfoOpt) {
        _otNameArea->clear();
        _nicknameArea->clear();
        _speciesArea->clear();
        _otIdArea->clear();
        if (detailedInfoOpt.has_value() && detailedInfoOpt != nullptr) {
            if (auto gen1Info = dynamic_cast<Gen1MonsterDetailViewModel*>(
                    detailedInfoOpt->get());
                gen1Info) {
                auto otName = gen1Info->getOtName();
                auto nick = gen1Info->getNickname();
                auto species = std::to_string(gen1Info->getSpecies());
                auto otId = std::to_string(gen1Info->getOtId());
                Expects(otId.size() <= 5);
                otId.insert(0, 5 - otId.size(), '0');
                constexpr std::array<std::optional<int>, 4> palette{
                    std::nullopt, 1, 2, std::nullopt};
                _otNameArea->write(otName, _font, palette);
                _nicknameArea->write(nick, _font, palette);
                _speciesArea->write(species, _font, palette);
                _otIdArea->write(otId, _font, palette);
            }
        }
    }

    void MonsterManagementView::Video::onSwapped(
        vm::MonPosition lhs, vm::MonPosition rhs, int activeBox) {
        for (const auto [_, box, positions] : std::array{lhs, rhs}) {
            if (box == activeBox) {
                for (auto monIndex : positions) {
                    _scrollingBox->redraw(monIndex);
                }
            }
        }
    }

    void MonsterManagementView::Video::positionCursorOnBackButton() {
        _cursor->object.get().x() = 225;
        _cursor->object.get().y() = 180;
    }

    void MonsterManagementView::Video::positionCursorOnBoxChangeButton(bool isLeft) {
        _cursor->object.get().x() = isLeft ? 0 : 180;
        _cursor->object.get().y() = 10;
    }

    void MonsterManagementView::Video::positionCursorOnBoxBody(int x, int y) {
        _cursor->object.get().x() = 24 * x + 24;
        _cursor->object.get().y() = 24 * y + 48;
    }
}
