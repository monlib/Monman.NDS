#include "usugatapc/ui/ScrollingBoxWidget.h"

#include "usugatapc/gfx/Font.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/hw/nds.h"

namespace usugatapc::ui {
    namespace {
        constexpr auto BoxHeightTiles = 20;
        constexpr auto BoxWidthTiles = 21;
    }

    ScrollingBoxWidget::ScrollingBoxWidget(vm::MonsterStorageViewModel* viewModel,
        gfx::Font font,
        gsl::span<halfword, 32> subpals,
        int bg,
        hw::VideoEngine engine,
        gsl::span<hw::OamBlock, 35> iconObjects,
        gsl::span<hw::Tile4[4 * 4], 35> iconSprites,
        gsl::span<hw::OamBlock, 6> headingObjects,
        gsl::span<hw::Tile4[4 * 2], 6> headingSprites) :
        _viewModel{viewModel},
        _font{font},
        _subpals{std::move(subpals)},
        _bg{std::move(bg)},
        _currSlot{0},
        _currColumn{0},
        _remainingScrollFrames{0},
        _monicons{make_not_null(viewModel),
            iconObjects,
            iconSprites,
            ::bgGetPriority(_bg)},
        _headingSprites{headingSprites},
        _headingObjects{headingObjects} {
        Expects(viewModel);

        initHeadings();
        positionHeading(0);
        loadBox();
        ::bgSetScroll(_bg, 4, -20);

        // Only subpal 3, 4, and 5 are relevant.
        loadFile("nitro:/graphics/monicons/icons.pal",
            engine == hw::VideoEngine::Main ? SPRITE_PALETTE : SPRITE_PALETTE_SUB);
    }

    void ScrollingBoxWidget::positionHeading(int x) {
        for (int i = 0; i < 3; ++i) {
            auto& entry = _headingObjects[i + 3 * _currSlot].entry;
            entry.objectMode() = hw::ObjectMode::Normal;
            entry.x() = x + i * 32;
            entry.y() = 16;
            fill(_headingSprites[i + 3 * _currSlot], hw::Tile4::solidColor(0));
        }
    }

    void ScrollingBoxWidget::loadHeading() {
        std::array<std::optional<int>, 4> pal{std::nullopt, 1, 2, std::nullopt};
        auto name = _viewModel->getCurrentBoxName().value_or("unknown");
        drawTextPerPixel(_font, name, [&](auto fontCol, auto fontX, auto fontY) {
            auto x = 5 + fontX;
            auto y = 0 + fontY;
            auto obj = x / 32;
            auto inObjX = x % 32;
            auto tileX = inObjX / 8;
            auto inTileX = inObjX % 8;
            auto tileY = y / 8;
            auto inTileY = y % 8;
            auto& tile = _headingSprites[_currSlot * 3 + obj][tileX + 4 * tileY];
            if (auto pxCol = pal[fontCol]; pxCol) {
                tile.writePixel(inTileX, inTileY, *pxCol);
            }
        });
    }

    void ScrollingBoxWidget::initHeadings() {
        for (auto& [entry, _] : _headingObjects) {
            hw::OamEntry newEntry{};
            newEntry.size() = hw::ObjectSize::k32;
            newEntry.shape() = hw::ObjectShape::Wide;
            newEntry.priority() = ::bgGetPriority(_bg);
            newEntry.objectMode() = hw::ObjectMode::Hidden;
            entry = newEntry;
        }
        for (int i = 0; i < _headingObjects.size(); ++i) {
            _headingObjects[i].entry.sprite() =
                spriteIndex(hw::SpriteBoundary::k128, _headingSprites[i]);
        }
    }

    void ScrollingBoxWidget::swapSlot() { _currSlot = !_currSlot; }

    void ScrollingBoxWidget::onBoxChanged(int change) {
        beginScrolling(change < 0 ? ScrollingBoxWidget::Direction::Left
                                  : ScrollingBoxWidget::Direction::Right);
    }

    void ScrollingBoxWidget::beginScrolling(Direction direction) {
        _currDirection = direction;
        moveTileColumn(direction);
        positionHeading(0 + alignedToDirection(BoxWidthTiles * 8, _currDirection));
        loadBox();
        startScrollFrameCount();
    }

    void ScrollingBoxWidget::advanceScrolling() {
        if (!isScrolling()) {
            return;
        }
        --_remainingScrollFrames;
        ::bgScroll(_bg, alignedToDirection(8, _currDirection), 0);
        _monicons.moveBy(alignedToDirection(-8, _currDirection));
        bool shouldLoadNewColumn =
            (_remainingScrollFrames % 3) == 0 && _remainingScrollFrames > 0;
        if (shouldLoadNewColumn) {
            auto columnsLeftToLoad = (_remainingScrollFrames / 3 - 1);
            auto columnInBox = _currDirection == Direction::Right
                ? 5 - columnsLeftToLoad
                : columnsLeftToLoad;
            auto relativeX = _currDirection == Direction::Right ? 18 * 8 : -24;
            _monicons.loadNewColumn(columnInBox, relativeX + 4);
            _monicons.changeUnusedColumnBy(alignedToDirection(1, _currDirection));
        }
        for (auto& [entry, _] : _headingObjects) {
            entry.x() += alignedToDirection(-8, _currDirection);
        }
    }

    void ScrollingBoxWidget::redraw(int index) { _monicons.redraw(index); }

    void ScrollingBoxWidget::loadBox() {
        loadBackground();
        loadHeading();
        swapSlot();
    }

    void ScrollingBoxWidget::loadBackground() {
        loadPalette("nitro:/graphics/boxbgs/Reshiram.pal");
        loadTiles("nitro:/graphics/boxbgs/Reshiram.bin");
        updateMap();
    }

    void ScrollingBoxWidget::loadPalette(const char* filename) {
        loadFile(filename, _subpals.data() + _currSlot * 16);
    }

    void ScrollingBoxWidget::loadTiles(const char* filename) {
        auto tiles =
            reinterpret_cast<hw::Tile4*>(::bgGetGfxPtr(_bg)) + _currSlot * 512;
        loadFile(filename, tiles);
    }

    int ScrollingBoxWidget::calculateSubpalIndex() const {
        return gsl::narrow<int>(
                   (reinterpret_cast<uintptr_t>(_subpals.data()) % 512) / 32)
            + _currSlot;
    }

    void ScrollingBoxWidget::updateMap() {
        auto subpalIndex = calculateSubpalIndex();
        gsl::span<hw::TileMapEntry16, 2048> map{
            reinterpret_cast<hw::TileMapEntry16*>(::bgGetMapPtr(_bg)), 2048};
        for (auto [x, y] : iterateRect(BoxWidthTiles, BoxHeightTiles)) {
            hw::TileMapEntry16 entry{
                x + y * BoxWidthTiles + _currSlot * 512, false, false, subpalIndex};
            auto realX = x + _currColumn;
            auto mapBase = realX / 32;
            auto xInMap = realX % 32;
            map[(xInMap + y * 32 + mapBase * 1024) % 2048] = entry;
        }
    }

    void ScrollingBoxWidget::moveTileColumn(Direction direction) {
        auto newColumn = _currColumn + alignedToDirection(BoxWidthTiles, direction);
        _currColumn = positiveModulo(newColumn, 64);
    }

    int ScrollingBoxWidget::alignedToDirection(int x, Direction d) {
        return (d == Direction::Right ? 1 : -1) * x;
    }

    void ScrollingBoxWidget::startScrollFrameCount() { _remainingScrollFrames = 21; }

    bool ScrollingBoxWidget::isScrolling() const {
        return _remainingScrollFrames != 0;
    }
}
