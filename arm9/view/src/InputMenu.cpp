
#include "usugatapc/ui/InputMenu.h"

namespace usugatapc::inputgraphdetail {
    NodeId SimpleNode::leave(NodeId, PosDetail, Direction d) const {
        switch (d) {
        case Direction::Up: return _up;
        case Direction::Down: return _down;
        case Direction::Left: return _left;
        case Direction::Right: return _right;
        default: Expects(false);
        }
    }
    auto SimpleNode::enter(NodeId, Position, Direction) const -> PosDetail {
        return PosDetail{};
    }
    SimpleNode::PosDetail SimpleNode::start() const { return PosDetail(); }

    SimpleNode::SimpleNode(NodeId up, NodeId down, NodeId left, NodeId right) :
        _up(up),
        _down(down),
        _left(left),
        _right(right) {}

    NodeId RectNode::leave(NodeId thisId, PosDetail pos, Direction d) const {
        Expects(0 <= pos.x && pos.x < width() && 0 <= pos.y && pos.y < height());
        switch (d) {
        case Direction::Up: {
            if (pos.y == 0) {
                return _vertical[pos.x].up;
            } else {
                return thisId;
            }
        }
        case Direction::Down: {
            if (pos.y == height() - 1) {
                return _vertical[pos.x].down;
            } else {
                return thisId;
            }
        }
        case Direction::Left: {
            if (pos.x == 0) {
                return _horizontal[pos.y].left;
            } else {
                return thisId;
            }
        }
        case Direction::Right: {
            if (pos.x == width() - 1) {
                return _horizontal[pos.y].right;
            } else {
                return thisId;
            }
        }
        default: Expects(false);
        }
    }

    auto RectNode::enter(
        NodeId thisId, Position prevPos, Direction fromDirection) const
        -> PosDetail {
        if (prevPos.node == thisId) {
            Expects(std::holds_alternative<PosDetail>(prevPos.posInfo));
            auto prevCoord = std::get<PosDetail>(prevPos.posInfo);
            auto newCoord = [&]() -> PosDetail {
                switch (fromDirection) {
                case Direction::Left:
                    if (1 <= prevCoord.x) {
                        return {prevCoord.x - 1, prevCoord.y};
                    } else {
                        return prevCoord;
                    }
                case Direction::Right:
                    if (prevCoord.x < width() - 1) {
                        return {prevCoord.x + 1, prevCoord.y};
                    } else {
                        return prevCoord;
                    }
                case Direction::Up:
                    if (1 <= prevCoord.y) {
                        return {prevCoord.x, prevCoord.y - 1};
                    } else {
                        return prevCoord;
                    }
                case Direction::Down:
                    if (prevCoord.y < height() - 1) {
                        return {prevCoord.x, prevCoord.y + 1};
                    } else {
                        return prevCoord;
                    }
                default: Expects(false);
                }
            }();
            Ensures(0 <= newCoord.x && newCoord.x < width() && 0 <= newCoord.y
                && newCoord.y < height());
            return newCoord;
        } else {
            return PosDetail{0, 0};
        }
    }
    RectNode::PosDetail RectNode::start() const { return PosDetail(); }

    RectNode::RectNode(
        std::vector<VertConn> vertical, std::vector<HorizConn> horizontal) :
        _vertical(std::move(vertical)),
        _horizontal(std::move(horizontal)) {
        Expects(!_vertical.empty() && !_horizontal.empty());
    }

    InputGraph::InputGraph(std::vector<Node> nodes) : _nodes{std::move(nodes)} {
        Expects(!_nodes.empty());
    }

    Position InputGraph::startAt(NodeId nodeId) const {
        auto posDetail = std::visit(
            [&](auto&& node) -> AnyPosDetail { return node.start(); }, get(nodeId));
        return {nodeId, posDetail};
    }
    Position InputGraph::moved(Position pos, Direction direction) const {
        auto newNode = std::visit(
            [&](auto&& node) -> NodeId {
                try {
                    using DetailType =
                        typename std::remove_reference_t<decltype(node)>::PosDetail;
                    auto posInfo = std::get<DetailType>(pos.posInfo);
                    return node.leave(pos.node, posInfo, direction);
                } catch (const std::bad_variant_access&) { Expects(false); }
            },
            get(pos.node));
        const AnyPosDetail& newInfo = std::visit(
            [&](auto&& node) -> AnyPosDetail {
                return node.enter(newNode, pos, direction);
            },
            get(newNode));
        return {newNode, newInfo};
    }
    const Node& InputGraph::get(NodeId id) const {
        Expects(0 <= id && id < gsl::narrow<int>(_nodes.size()));
        return _nodes[id];
    }

    bool Position::operator==(const Position& rhs) const {
        return node == rhs.node && posInfo == rhs.posInfo;
    }

    bool Position::operator!=(const Position& rhs) const { return !(rhs == *this); }

    bool RectNode::PosDetail::operator==(const RectNode::PosDetail& rhs) const {
        return x == rhs.x && y == rhs.y;
    }

    bool RectNode::PosDetail::operator!=(const RectNode::PosDetail& rhs) const {
        return !(rhs == *this);
    }
}
