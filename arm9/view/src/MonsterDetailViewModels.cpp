#include "usugatapc/MonsterDetailViewModels.h"

#include <string>

#include <gsl/gsl>

namespace usugatapc {
    namespace {}

    Gen1MonsterDetailViewModel::Gen1MonsterDetailViewModel(sol::table monster) :
        _monster{std::move(monster)} {
        Expects(_monster);
    }

    std::string Gen1MonsterDetailViewModel::getNickname() {
        return _monster["nickname"];
    }

    std::string Gen1MonsterDetailViewModel::getOtName() {
        return _monster["originalTrainerName"];
    }

    int Gen1MonsterDetailViewModel::getSpecies() { return _monster["species"]; }

    int Gen1MonsterDetailViewModel::getOtId() {
        return _monster["originalTrainerId"];
    }
}
