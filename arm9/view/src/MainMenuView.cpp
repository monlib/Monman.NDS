#include "usugatapc/ui/MainMenuView.h"

#include "usugatapc/Utility.h"
#include "usugatapc/gfx/Font.h"
#include "usugatapc/gfx/Text.h"
#include "usugatapc/hw/Video.h"
#include "usugatapc/hw/nds.h"
#include "usugatapc/ui/CreditsView.h"
#include "usugatapc/ui/MonsterManagementView.h"
#include "utf8.h"

namespace usugatapc::ui {
    namespace {
        enum ButtonIndexes {
            MonsterButtonIndex,
            CreditsButtonIndex,
            QuitButtonIndex,
            ButtonCount,
        };
        constexpr std::array<std::optional<int>, 4> fontPalette{
            std::nullopt,
            1,
            2,
            std::nullopt,
        };
        constexpr auto FirstHighlightSprite = 0;
        constexpr auto HighlightSpriteCount = 4;
        constexpr auto HighlightTileIndex = 0;

        struct HighlightOffsets {
            int top, bottom, left, right;
        };
        constexpr HighlightOffsets Offsets{-2, -6, 6, 2};

        void moveHighlightsTo(int top, int bottom, int left, int right) {
            for (int i = 0; i < HighlightSpriteCount; ++i) {
                auto x = i % 2 == 0 ? left + Offsets.left : right + Offsets.right;
                auto y = i / 2 == 0 ? top + Offsets.top : bottom + Offsets.bottom;
                ::oamSetXY(&::oamSub, i + FirstHighlightSprite, x, y);
            }
        }
    }

    MainMenuView::MainMenuView(std::shared_ptr<vm::MainMenuViewModel> vm) :
        _video{initializedLater},
        _vm{std::move(vm)},
        _currSelection{0} {}

    void MainMenuView::update() {
        if (_video == initializedLater) {
            _video = Video();
        }
        ::scanKeys();
        auto keys = ::keysDown();
        moveHighlight(keys);
        _video->moveHighlightsToSelection(_currSelection);
        if (keys & KEY_A) {
            switch (_currSelection) {
            case MonsterButtonIndex: switchToMonsterView(); break;
            case CreditsButtonIndex: switchToCredits(); break;
            case QuitButtonIndex: setNextRootView(nullptr); break;
            default: break;
            }
        }
        ::swiWaitForVBlank();
        ::bgUpdate();
        ::oamUpdate(&::oamSub);
    }

    void MainMenuView::moveHighlight(uint32_t keys) {
        int change = [=]() {
            if (keys & KEY_UP) {
                return -1;
            } else if (keys & KEY_DOWN) {
                return 1;
            } else {
                return 0;
            }
        }();
        _currSelection = std::abs((_currSelection + change) % ButtonCount);
    }

    void MainMenuView::switchToMonsterView() {
        auto monsterVm = _vm->openMonsterManager();
        Expects(monsterVm != nullptr);  // TODO: Handle this gracefully
        std::function<std::shared_ptr<RootView>(
            const vm::MonsterManagementViewModel&)>
            onFinished = [vm = _vm](const vm::MonsterManagementViewModel& monManVm)
            -> std::shared_ptr<RootView> {
            vm->applyMonsterStorageChanges(monManVm);
            return std::make_shared<MainMenuView>(vm);
        };
        setNextRootView(std::make_shared<MonsterManagementView>(
            std::move(monsterVm), std::move(onFinished)));
    }

    void MainMenuView::switchToCredits() {
        setNextRootView(std::make_shared<CreditsView>(
            gsl::not_null<const char*>{"nitro:/credits.txt"},
            [vm = _vm]() { return std::make_shared<MainMenuView>(vm); }));
    }

    MainMenuView::Video::Video() {
        ::lcdMainOnTop();
        ::videoSetMode(MODE_0_2D);
        ::videoSetModeSub(MODE_0_2D);
        ::vramSetBankA(::VRAM_A_MAIN_BG);
        ::vramSetBankB(::VRAM_B_MAIN_SPRITE);
        ::vramSetBankC(::VRAM_C_SUB_BG);
        ::vramSetBankD(::VRAM_D_SUB_SPRITE);

        std::array<int, 4> subBgs{
            ::bgInitSub(0, ::BgType_Text4bpp, ::BgSize_T_256x256, 0, 0),
            ::bgInitSub(1, ::BgType_Text4bpp, ::BgSize_T_256x256, 2, 3),
            ::bgInitSub(2, ::BgType_Text4bpp, ::BgSize_T_256x256, 0, 1),
            ::bgInitSub(3, ::BgType_Text4bpp, ::BgSize_T_256x256, 1, 1),
        };

        loadFile("nitro:/ui/mainmenu-Layer2.pal", BG_PALETTE_SUB);
        loadFile("nitro:/ui/mainmenu-Layer2.tiles", ::bgGetGfxPtr(subBgs[2]));
        loadFile("nitro:/ui/mainmenu-Layer2.map", ::bgGetMapPtr(subBgs[2]));
        loadFile("nitro:/ui/mainmenu-Layer3.map", ::bgGetMapPtr(subBgs[3]));

        gfx::Font font =
            gfx::Font{loadFileShared<fullword>("nitro:/fonts/default.bin")};
        gsl::span textMap{
            reinterpret_cast<hw::TileMapEntry16*>(::bgGetMapPtr(subBgs[1])), 1024};
        gsl::span textGfx{
            reinterpret_cast<hw::Tile4*>(::bgGetGfxPtr(subBgs[1])), 512};
        // blank bg1
        using std::begin, std::end;
        std::fill(
            begin(textMap), end(textMap), hw::TileMapEntry16{0, false, false, 0});
        textGfx[0] = hw::Tile4::solidColor(0);

        constexpr gfx::TextAreaDescriptor monsterTextDesc{3, 18, 8, 4, 8, 4, 1};
        gfx::drawText(monsterTextDesc, subBgs[1], 2, "MONSTER", font, fontPalette);
        constexpr gfx::TextAreaDescriptor creditsTextDesc{3, 18, 8, 8, 8, 4, 55};
        gfx::drawText(creditsTextDesc, subBgs[1], 2, "Credits", font, fontPalette);

        // Prepare highlights
        ::oamInit(&::oamSub, ::SpriteMapping_1D_32, false);
        ::oamDisable(&::oamSub);
        loadFile("nitro:/ui/highlight.pal", SPRITE_PALETTE_SUB);
        loadFile("nitro:/ui/highlight.sprite",
            ::oamGetGfxPtr(&::oamSub, HighlightTileIndex));
        for (int i = 0; i < HighlightSpriteCount; ++i) {
            auto hflip = static_cast<bool>(i % 2);
            auto vflip = static_cast<bool>(i / 2);
            ::oamSet(&::oamSub,
                FirstHighlightSprite + i,  // id
                0,  // x
                0,  // y
                0,  // priority
                0,  // palette
                ::SpriteSize_8x8,
                ::SpriteColorFormat_16Color,
                ::oamGetGfxPtr(&::oamSub, HighlightTileIndex),
                -1,  // affineIndex
                false,  // N/A
                false,  // not hidden
                hflip,
                vflip,
                false);  // not mosaic
        }
        moveHighlightsTo(32, 32 + 24, 56, 56 + 144);

        // Enable video output
        ::bgHide(subBgs[0]);
        ::bgShow(subBgs[1]);
        ::bgShow(subBgs[2]);
        ::bgShow(subBgs[3]);
        ::oamEnable(&::oamSub);
    }

    void MainMenuView::Video::moveHighlightsToSelection(int selection) {
        switch (selection) {
        case MonsterButtonIndex: moveHighlightsTo(32, 32 + 24, 56, 56 + 144); break;
        case CreditsButtonIndex: moveHighlightsTo(64, 64 + 24, 56, 56 + 144); break;
        case QuitButtonIndex: moveHighlightsTo(160, 160 + 24, 184, 184 + 24); break;
        default: Expects(false);
        }
    }
}
