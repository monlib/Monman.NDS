#include "usugatapc/hw/Video.h"

#include "usugatapc/hw/nds.h"

// Place g_mainOam and g_subOam at their respective addresses
asm("g_usugatapc_hw_mainOam = 0x07000000");
asm("g_usugatapc_hw_subOam = 0x07000400");

namespace usugatapc::hw {
    namespace {}

    void initOam(VideoEngine engine, int mapping, bool enableExtPal) {
        auto& reg = ((engine == VideoEngine::Main) ? REG_DISPCNT : REG_DISPCNT_SUB);
        reg &= ~DISPLAY_SPRITE_ATTR_MASK;
        reg |= DISPLAY_SPR_ACTIVE | (mapping & 0x0FFF'FFF0) | enableExtPal << 31;
    }

    void OamShadow::copyToOam(VideoEngine engine) {
        if (engine == VideoEngine::Main) {
            copy(blocks, g_mainOam);
        } else {
            copy(blocks, g_subOam);
        }
    }
}
