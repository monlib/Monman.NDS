#include "usugatapc/gfx/Text.h"

namespace usugatapc::gfx {
    TextArea::TextArea(TextAreaDescriptor desc, int bg, int subpal) :
        TextArea(desc,
            gsl::span{
                reinterpret_cast<hw::TileMapEntry16*>(::bgGetMapPtr(bg)), 1024},
            gsl::span{reinterpret_cast<hw::Tile4*>(::bgGetGfxPtr(bg)), 512}.subspan(
                desc.firstTile, desc.height * desc.width),
            subpal) {}

    TextArea::TextArea(TextAreaDescriptor desc,
        gsl::span<hw::TileMapEntry16> map,
        gsl::span<hw::Tile4> gfx,
        int subpal) :
        _desc{desc},
        _gfx{gfx} {
        init(_desc.mapX,
            _desc.mapY,
            _desc.width,
            _desc.height,
            _desc.firstTile,
            subpal,
            map);
    }

    void TextArea::clear() { fill(_gfx, hw::Tile4::solidColor(0)); }

    void TextArea::write(std::string_view text,
        const Font& font,
        std::array<std::optional<int>, 4> pal) {
        draw(_desc.textX, _desc.textY, text, font, _desc.height, _gfx, pal);
    }

    void TextArea::draw(int baseX,
        int baseY,
        std::string_view text,
        const Font& font,
        int areaHeight,
        gsl::span<hw::Tile4> tiles,
        std::array<std::optional<int>, 4> pal) {
        drawTextPerPixel(font, text, [&](auto fontCol, auto x, auto y) {
            auto inAreaX = baseX + x;
            auto inAreaY = baseY + y;
            auto tileIndex = (inAreaX / 8) * areaHeight + (inAreaY / 8);
            auto& tile = tiles[tileIndex];
            if (auto pxCol = pal[fontCol]; pxCol) {
                tile.writePixel(inAreaX % 8, inAreaY % 8, *pxCol);
            }
        });
    }

    void TextArea::init(int baseX,
        int baseY,
        int width,
        int height,
        int firstIndex,
        int subpalette,
        gsl::span<hw::TileMapEntry16> mapBase) {
        for (auto [x, y] : iterateRect(width, height)) {
            mapBase[32 * (y + baseY) + x + baseX] = hw::TileMapEntry16{
                gsl::narrow<uint16_t>(firstIndex + x * height + y),
                false,
                false,
                subpalette,
            };
        }
    }

    void drawText(const TextAreaDescriptor& desc,
        int bg,
        int subpal,
        std::string_view text,
        const Font& font,
        std::array<std::optional<int>, 4> pal) {
        TextArea area(desc, bg, subpal);
        area.clear();
        area.write(text, font, pal);
    }
}