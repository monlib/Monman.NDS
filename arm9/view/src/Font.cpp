#include "usugatapc/gfx/Font.h"

#include <climits>

namespace usugatapc::gfx {
    namespace {
        constexpr auto VeryLargeTableSize = 10000;
        constexpr auto FontBpp = 2;
        constexpr auto FontPxPerHalfword = CHAR_BIT * sizeof(halfword) / FontBpp;
    }

    GlyphView::GlyphView(const halfword* data) : _data{data} {
        Expects(data != nullptr);
    }

    int GlyphView::getHeight() const { return getHeader() & 0x000F; }
    int GlyphView::getWidth() const { return (getHeader() & 0x00F0) >> 4; }
    int GlyphView::getYOffset() const {
        // Shift left to preserve sign when right-shifting afterwards.
        return ((getHeader() & 0x0F00) << 4) >> 12;
    }
    int GlyphView::getSpacing() const { return (getHeader() & 0xF000) >> 12; }
    int16_t GlyphView::getHeader() const { return static_cast<int16_t>(_data[0]); }
    int GlyphView::getPixelCount() const { return getHeight() * getWidth(); }
    int GlyphView::getPixel(int index) const {
        auto halfword = static_cast<uint16_t>(_data[1 + index / FontPxPerHalfword]);
        return (halfword >> (index % FontPxPerHalfword * FontBpp)) & 0b11;
    }
    int GlyphView::getPixel(int x, int y) const {
        return getPixel(y * getWidth() + x);
    }

    Font::Font(Monlib::Util::shared_dynarray<const fullword> data) :
        _data{std::move(data)} {
        Expects(1 < _data.size());
        auto tableSize = static_cast<int>(_data[0]);
        Expects(0 < tableSize && tableSize < VeryLargeTableSize);
        auto sizeFieldSize = 1;
        auto* convTablePtr = reinterpret_cast<const UnicodeGlyphMapEntry*>(
            _data.data() + sizeFieldSize);
        _conversionTable = gsl::span{convTablePtr, tableSize};
        int glyphTableOffset = sizeFieldSize
            + tableSize * sizeof(UnicodeGlyphMapEntry) / sizeof(fullword);
        auto* glyphTablePtr =
            reinterpret_cast<const halfword*>(_data.data() + glyphTableOffset);
        _glyphTable = gsl::span{glyphTablePtr, _data.size() - glyphTableOffset};
        Ensures(_data != nullptr && _data.data() != nullptr);
    }

    std::optional<GlyphView> Font::getGlyph(int codepoint) const {
        using std::begin, std::end;
        auto firstMatch = std::lower_bound(begin(_conversionTable),
            end(_conversionTable),
            codepoint,
            [](auto& a, auto& b) { return a.codepoint < b; });

        if (firstMatch != end(_conversionTable)
            && firstMatch->codepoint == codepoint) {
            return GlyphView{_glyphTable.data() + firstMatch->glyphIndex};
        } else {
            return std::nullopt;
        }
    }
}
