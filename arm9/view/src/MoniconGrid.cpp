#include "usugatapc/ui/MoniconGrid.h"

namespace usugatapc::ui {
    namespace {
        constexpr auto MoniconsMetadataPath = "nitro:/graphics/monicons/m";
        constexpr auto MoniconsGraphicsPath = "nitro:/graphics/monicons/d";
        constexpr auto MoniconsColumnCount = 7;
        constexpr auto MoniconsVisibleColumnCount = 6;
        constexpr auto MoniconsRowCount = 5;
        constexpr auto MoniconsVisibleCount =
            MoniconsRowCount * MoniconsVisibleColumnCount;
        constexpr auto YOffset = 43;  // This should be parameterized
    }

    MoniconGrid::MoniconGrid(gsl::not_null<vm::MonsterStorageViewModel*> viewModel,
        gsl::span<hw::OamBlock, 35> objects,
        gsl::span<hw::Tile4[4 * 4], 35> sprites,
        int priority) :
        _viewModel{viewModel},
        _graphicsFile{std::fopen(MoniconsGraphicsPath, "rb")},
        _unusedColumn{6},
        _objects{objects},
        _sprites{sprites} {
        Expects(_graphicsFile);
        loadMetadata();
        initializeObjects(priority);
        positionObjects();
        loadSprites();
    }

    void MoniconGrid::loadMetadata() {
        UniqueFile metadata{std::fopen(MoniconsMetadataPath, "rb")};
        Expects(std::fread(
            &_totalIconCount, sizeof(_totalIconCount), 1, metadata.get()));
        Expects(std::fread(
            &_regularIconCount, sizeof(_regularIconCount), 1, metadata.get()));
        Expects(_totalIconCount >= _regularIconCount);
        constexpr auto eggIconCount = 1;
        auto specialIconCount = _totalIconCount - _regularIconCount - eggIconCount;
        _index = Monlib::Util::make_dynarray<IconIndex>(specialIconCount);
        auto read = std::fread(_index.data(),
            sizeof(IconIndex),
            gsl::narrow<std::size_t>(specialIconCount),
            metadata.get());
        Expects(gsl::narrow<int>(read) == specialIconCount);
    }

    void MoniconGrid::initializeObjects(int priority) {
        for (auto [x, y] : iterateRect(MoniconsColumnCount, MoniconsRowCount)) {
            hw::OamEntry entry{};
            entry.priority() = priority;
            entry.size() = hw::ObjectSize::k32;
            entry.shape() = hw::ObjectShape::Square;
            entry.sprite() = spriteIndex(hw::SpriteBoundary::k128, getSprite(x, y));
            getObject(x, y) = entry;
        }
    }

    void MoniconGrid::redraw(int boxIndex) {
        auto infoOpt = getBriefInfo(boxIndex);
        auto gfxIdOpt = findSpriteIndex(infoOpt);
        auto column = (_unusedColumn + 1 + boxIndex % MoniconsVisibleColumnCount)
            % MoniconsColumnCount;
        auto row = boxIndex / MoniconsVisibleColumnCount;
        loadGraphics(column, row, gfxIdOpt);
    }

    void MoniconGrid::positionObjects() {
        for (int x = 0; x < MoniconsColumnCount; ++x) {
            positionColumn(x, x * 24);
        }
    }

    void MoniconGrid::loadSprites() {
        for (int i = 0; i < MoniconsVisibleColumnCount; ++i) {
            loadColumnGraphics(i, i);
        }
    }

    void MoniconGrid::moveBy(int x) {
        for (auto& [entry, _] : _objects) {
            entry.x() += x;
        }
    }

    void MoniconGrid::loadNewColumn(int columnInBox, int x) {
        positionColumn(_unusedColumn, x);
        loadColumnGraphics(columnInBox, _unusedColumn);
    }

    void MoniconGrid::changeUnusedColumnBy(int amount) {
        Expects(amount == -1 || amount == 1);
        _unusedColumn = positiveModulo(_unusedColumn + amount, MoniconsColumnCount);
    }

    void MoniconGrid::positionColumn(int col, int x) {
        for (int y = 0; y < MoniconsRowCount; ++y) {
            auto& entry = getObject(col, y);
            entry.x() = x;
            entry.y() = y * 24 + YOffset;
        }
    }

    void MoniconGrid::loadColumnGraphics(int visualColumn, int resourceColumn) {
        // This function runs each stage of the pipeline for all monsters to
        // completion before starting the next step of the pipeline. This is faster
        // than running the whole pipeline at once for each monster, since it reduces
        // cache misses.
        using vm::BriefMonInfo;
        std::array<int, MoniconsRowCount> rows{};
        iota(rows, 0);
        std::array<int, MoniconsRowCount> boxIndexes{};
        transform(rows, boxIndexes, [visualColumn](auto i) {
            return i * MoniconsVisibleColumnCount + visualColumn;
        });
        std::array<std::optional<BriefMonInfo>, MoniconsRowCount> infoOpts{};
        transform(boxIndexes, infoOpts, [&](auto i) { return getBriefInfo(i); });
        std::array<std::optional<int>, MoniconsRowCount> gfxIds{};
        transform(infoOpts, gfxIds, [&](auto i) { return findSpriteIndex(i); });
        std::array<std::tuple<int, std::optional<int>>, MoniconsRowCount> loadData{};
        transform(rows, gfxIds, loadData, [](auto a, auto b) {
            return std::tuple{a, b};
        });
        for (auto [row, gfxIdOpt] : loadData) {
            loadGraphics(resourceColumn, row, gfxIdOpt);
        }
    }

    std::optional<int> MoniconGrid::findSpriteIndex(
        std::optional<vm::BriefMonInfo> infoOpt) const {
        using std::begin, std::end;
        if (infoOpt.has_value()) {
            auto info = *infoOpt;
            if (info.isEgg) {
                return _totalIconCount - 1;
            } else if (info.form == 0 && 0 <= info.species
                && info.species < _regularIconCount) {
                return info.species;
            } else {
                IconIndex searched{info.species, info.form};
                auto firstNotLess =
                    std::lower_bound(begin(_index), end(_index), searched);
                if (firstNotLess != end(_index) && *firstNotLess == searched) {
                    return _regularIconCount + (firstNotLess - begin(_index));
                } else {
                    return 0;
                }
            }
        } else {
            return std::nullopt;
        }
    }

    void MoniconGrid::loadGraphics(int column, int row, std::optional<int> gfxId) {
        auto& entry = getObject(column, row);
        entry.objectMode() =
            gfxId.has_value() ? hw::ObjectMode::Normal : hw::ObjectMode::Hidden;
        if (gfxId) {
            loadSprite(entry, getSprite(column, row), *gfxId);
        }
    }

    void MoniconGrid::loadSprite(
        hw::OamEntry& entry, hw::Tile4 (&tiles)[4 * 4], int iconIndex) {
        Expects(std::fseek(_graphicsFile.get(),
                    iconIndex * (sizeof(hw::Tile4) * 4 * 4 + sizeof(int)),
                    SEEK_SET)
            == 0);
        int subpalette;
        Expects(std::fread(&subpalette, sizeof(int), 1, _graphicsFile.get()));
        Expects(
            std::fread(&tiles, sizeof(hw::Tile4) * 4 * 4, 1, _graphicsFile.get()));
        entry.palette() = subpalette;
    }

    hw::OamEntry& MoniconGrid::getObject(int x, int y) {
        return _objects[y + x * MoniconsRowCount].entry;
    }

    // Returns a reference to a 4*4 array of Tile4
    hw::Tile4 (&MoniconGrid::getSprite(int x, int y))[4 * 4] {
        return _sprites[y + x * MoniconsRowCount];
    }

    std::optional<vm::BriefMonInfo> MoniconGrid::getBriefInfo(int i) {
        return _viewModel->getBriefInfo(i);
    }
}
