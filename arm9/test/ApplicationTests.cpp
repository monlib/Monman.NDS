#include <type_traits>

#include "catch.hpp"
#include "lua.hpp"

#include "usugatapc/vm/Application.h"
#include "usugatapc/vm/sol.h"

using namespace usugatapc::vm;

SCENARIO("An Application is used to perform setup") {
    GIVEN("An application object") {
        Application app{"monswap.lua"};
        WHEN("A save loader has been set") {
            app.registerLoaders([](sol::state_view lua) -> sol::protected_function {
                return lua.script("return function(data) return {} end");
            });
            THEN("A save loader can be requested") {
                static_cast<void>(SaveLoaderViewModel{app.getSaveLoader()});
            }
        }
    }
}
