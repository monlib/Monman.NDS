local luagame = {}

-- Forward declarations
local serialize
local shallowCopy
local varIndex


-- Constants
luagame.monsterFormat = "\x44\xb1\xc1\x5f\xca\x21\x49\xc6\xa6\x39\x60\x84\xb5\xa2\x92\x64"



luagame.Save = {}
luagame.Save.__index = luagame.Save

function luagame.Save.FromProps(properties)
    return setmetatable(shallowCopy(properties), luagame.Save)
end

function luagame.Save:withMonsterStorage(storage)
    if getmetatable(storage) == luagame.Storage then
        return luagame.Save.FromProps{monsterStorage=storage}
    else
        return nil, "Not a luagame.MonsterStorage object"
    end
end

function luagame.Save:serializeToFile(file)
    file:write(self:serializeToString())
end

function luagame.Save:serializeToString()
    return serialize(self)
end



luagame.MonsterStorage = {}
luagame.MonsterStorage.__index = luagame.MonsterStorage

function luagame.MonsterStorage.FromProps(properties)
    return setmetatable(shallowCopy(properties), luagame.MonsterStorage)
end

function luagame.MonsterStorage:withBoxAt(box, ...)
    local keys = { ... }
    local n = select("#", ...)
    if n ~= 1 then return nil, "Too many keys (for now)" end
    if keys[1] <= 0 or #self < keys[1] then return nil, "Key out of range" end
    local newStorage = self:_copyButShareBoxes()
    local success, objToInsertAt = pcall(varIndex, newStorage, table.unpack(keys, 1, n - 1))
    if not success or type(objToInsertAt) ~= "table" then return nil, "Failed to look up keys" end
    objToInsertAt[keys[n]] = box
    return newStorage
end

function luagame.MonsterStorage:_copyButShareBoxes()
    return setmetatable(shallowCopy(self), luagame.MonsterStorage)
end



luagame.Box = {}
luagame.Box.__index = luagame.Box

function luagame.Box.FromProps(properties)
    return setmetatable(shallowCopy(properties), luagame.Box)
end

luagame.Box.acceptedBinaryFormats = { luagame.monsterFormat }

function luagame.Box:withMonAt(monster, index)
    if type(monster) ~= "table" and type(monster) ~= "userdata" and monster ~= nil then
        return nil, string.format("monster has invalid type %q", type(monster))
    end
    if type(index) ~= "number" or not self:_indexIsValid(index) then
        return nil, string.format("index is invalid")
    end
    local newBox = shallowCopy(self)
    newBox[index] = monster
    return setmetatable(newBox, luagame.Box)
end

function luagame.Box:_indexIsValid(i)
    if self.capacity then
        return 0 < i and i <= self.capacity
    else
        return 0 < i
    end
end



luagame.Monster = {}
luagame.Monster.__index = luagame.Monster

function luagame.Monster.FromProps(properties)
    return setmetatable(shallowCopy(properties), luagame.Monster)
end

-- This is *very* insecure, but it's just test code, so it doesn't matter
function luagame.Monster.Deserialize(serialized)
    return luagame.Monster.FromProps(load("return " .. serialized)())
end

luagame.Monster.binaryFormats = { luagame.monsterFormat, }

function luagame.Monster:serializeTo(formatTag)
    if formatTag == luagame.monsterFormat then
        return serialize(self)
    else
        return nil, "Unknown format!"
    end
end


-- Helper functions
shallowCopy = function (source, dest)
    dest = dest or {}
    for k, v in pairs(source) do dest[k] = v end
    return dest
end

serialize = function (o)
    local t = type(o)
    if t == "number" or t == "boolean" or t == "nil" then
        return tostring(o)
    elseif t == "string" then
        return string.format("%q", o)
    elseif t == "table" then
        local str = "{"
        for k, v in pairs(o) do
            str = string.format("%s[%s]=%s,", str, serialize(k), serialize(v))
        end
        return str .. "}"
    else
        error(string.format("Cannot serialize object of type %s!", t))
    end
end

varIndex = function (t, ...)
    if select("#", ...) == 0 then return t
    else return varIndex(t[select(1, ...)], select(2, ...)) end
end


return luagame
