#include "catch.hpp"

#include "usugatapc/vm/MonsterManagementViewModel.h"

using namespace usugatapc::vm;

namespace {
    std::shared_ptr<MonsterManagementViewModel> createMonsterManagementVMWith(
        std::string code) {
        auto lua = std::make_shared<sol::state>();
        openAllLuaLibraries(*lua);
        sol::table storage = lua->script(code);
        MonsterManagementFactoryImpl make{
            *lua, "monswap.lua", std::make_shared<MonsterDetailFactories>()};
        return make(std::move(lua), std::move(storage));
    }

    struct MockHandler : public MonsterManagementEventHandler {
        void onSwapped(MonPosition, MonPosition) override { swapped = true; }
        bool swapped = false;
    };
}

SCENARIO("A MonsterManagementVM is used to select and move mons") {
    GIVEN("A VM for a MonsterStorage with one box and a few mons") {
        auto monMan = createMonsterManagementVMWith(R"(
            local luagame = require("luagame")
            return luagame.MonsterStorage.FromProps{
                [1] = luagame.Box.FromProps{
                    name = "box1",
                    capacity = 10,
                    [1] = luagame.Monster.FromProps{ species = 1, },
                    [3] = luagame.Monster.FromProps{ species = 3, },
                    [7] = luagame.Monster.FromProps{ species = 7, },
                },
            }
        )");
        auto handler = std::make_shared<MockHandler>();
        monMan->setHandler(handler);
        WHEN("The first and third mon are swapped") {
            std::array lpos{0}, rpos{2};
            monMan->selectMonsters({Storage::Primary, 0, lpos});
            monMan->selectMonsters({Storage::Primary, 0, rpos});
            THEN("Their indexes are swapped") {
                auto& storage = *monMan->getPrimaryStorage();
                REQUIRE(handler->swapped);
                REQUIRE(storage.getBriefInfo(0).value().species == 3);
                REQUIRE(storage.getBriefInfo(2).value().species == 1);
            }
        }
        WHEN("The first mon and the second empty spot are swapped") {
            std::array lpos{0}, rpos{1};
            monMan->selectMonsters({Storage::Primary, 0, lpos});
            monMan->selectMonsters({Storage::Primary, 0, rpos});
            THEN("The mon and the empty spot have swapped places") {
                auto& storage = *monMan->getPrimaryStorage();
                REQUIRE(handler->swapped);
                REQUIRE(!storage.getBriefInfo(0).has_value());
                REQUIRE(storage.getBriefInfo(1).value().species == 1);
            }
        }
    }
    GIVEN("A VM for a monster storage with two boxes and some mons") {
        auto monMan = createMonsterManagementVMWith(R"(
            local luagame = require("luagame")
            return luagame.MonsterStorage.FromProps{
                [1] = luagame.Box.FromProps{
                    name = "box1",
                    capacity = 10,
                    [1] = luagame.Monster.FromProps{ species = 1, },
                },
                [2] = luagame.Box.FromProps{
                    name = "box2",
                    capacity = 5,
                    [4] = luagame.Monster.FromProps{ species = 4, },
                },
            })");
        auto handler = std::make_shared<MockHandler>();
        monMan->setHandler(handler);
        WHEN("The two monsters are swapped") {
            std::array lpos{0}, rpos{3};
            monMan->selectMonsters({Storage::Primary, 0, lpos});
            monMan->selectMonsters({Storage::Primary, 1, rpos});
            THEN("That's exactly what happens") {
                auto& storage = *monMan->getPrimaryStorage();
                REQUIRE(handler->swapped);
                REQUIRE(storage.getBriefInfo(0).value().species == 4);
                storage.changeBoxBy(1);
                REQUIRE(storage.getBriefInfo(3).value().species == 1);
            }
        }
    }
}
