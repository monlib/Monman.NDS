#include "catch.hpp"

#include "usugatapc/vm/MainMenuViewModel.h"

using namespace usugatapc::vm;

namespace {
    MainMenuViewModel createMainMenuWith(std::string script) {
        auto lua = std::make_shared<sol::state>();
        openAllLuaLibraries(*lua);
        auto save = lua->safe_script(script);
        auto makeMonsterManagement = std::make_shared<MonsterManagementFactoryImpl>(
            *lua, "monswap.lua", std::make_shared<MonsterDetailFactories>());
        return MainMenuViewModel{
            std::move(lua), std::move(save), std::move(makeMonsterManagement)};
    }
}

SCENARIO("A main menu lets the user select functionality") {
    GIVEN("A main menu for a save without monster storage") {
        auto menu = createMainMenuWith(R"(return { monsterStorage = nil, })");
        WHEN("The monster storage is requested") {
            auto storage = menu.openMonsterManager();
            THEN("nullptr is returned") { REQUIRE(storage == nullptr); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage errors") {
        auto menu = createMainMenuWith(R"(
            local t = { __index = function(self, k) error(k) end, }
            return setmetatable(t, t))");
        WHEN("The monster storage is requested") {
            auto storage = menu.openMonsterManager();
            THEN("nullptr is returned") { REQUIRE(storage == nullptr); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage has the wrong type") {
        auto menu = createMainMenuWith(R"(return { monsterStorage = 5, })");
        WHEN("The monster storage is requested") {
            auto storage = menu.openMonsterManager();
            THEN("nullptr is returned") { REQUIRE(storage == nullptr); }
        }
    }

    GIVEN("A main menu for a save where monsterStorage exists") {
        auto menu =
            createMainMenuWith(R"(return { monsterStorage = { [1] = {}, }, })");
        WHEN("The monster storage is requested") {
            std::shared_ptr<MonsterManagementViewModel> storage =
                menu.openMonsterManager();
            THEN("One is returned") { REQUIRE(storage != nullptr); }
        }
    }
}
