TEST_BUILD_OPTS := -pr monlib-base -s build_type=Debug -o usugataPC-ARM9:withTests=True
ARM9_BUILD_OPTS := -o lua:bits32=True -pr nds-arm9 -s build_type=Release
NDS_BUILD_OPTS := -pr nds

CODE_DIR := arm9
NDS_DIR := nds

# Intermediate package name
ARM9_REF := usugataPC-ARM9/current@monlib/intermediate

ARM9_BUILD_DIR := build/arm9/build
ARM9_SOURCE_DIR := build/arm9/source
NDS_BUILD_DIR := build/nds/build
NDS_SOURCE_DIR := $(NDS_BUILD_DIR) # Makefile is not set up for out-of-source build
TEST_BUILD_DIR := build/test/build # Shares arm9 source

.PHONY: all nds arm9 test clean clean-arm9 clean-nds clean-test

all: arm9 nds test

clean: clean-arm9 clean-nds clean-test

clean-arm9:
	rm -rf $(ARM9_BUILD_DIR) $(ARM9_SOURCE_DIR)
	conan remove --force $(ARM9_REF)

clean-nds:
	rm -rf $(NDS_BUILD_DIR) ./usugataPC.nds
clean-test:
	rm -rf $(TEST_BUILD_DIR) $(ARM9_SOURCE_DIR)

arm9:
	conan install  --install-folder $(ARM9_BUILD_DIR) --build missing $(ARM9_BUILD_OPTS) $(CODE_DIR)
	conan source --install-folder $(ARM9_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR)
	conan build --build-folder $(ARM9_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR)
ifndef NO_EXPORT_PACKAGE
	conan export-pkg --force --build-folder $(ARM9_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR) $(ARM9_REF)
endif

nds:
	conan install  --install-folder $(NDS_BUILD_DIR) $(NDS_BUILD_OPTS) $(NDS_DIR)
	conan source --install-folder $(NDS_BUILD_DIR) --source-folder $(NDS_BUILD_DIR) $(NDS_DIR)
	conan build --build-folder $(NDS_BUILD_DIR) --source-folder $(NDS_BUILD_DIR) $(NDS_DIR)
	@cp $(NDS_BUILD_DIR)/usugataPC.nds .

test:
	conan install  --install-folder $(TEST_BUILD_DIR) --build missing $(TEST_BUILD_OPTS) $(CODE_DIR)
	conan source --install-folder $(TEST_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR)
	conan build --build-folder $(TEST_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR)
ifndef NO_EXPORT_PACKAGE
	conan export-pkg --force --build-folder $(TEST_BUILD_DIR) --source-folder $(ARM9_SOURCE_DIR) $(CODE_DIR) $(ARM9_REF)
endif
