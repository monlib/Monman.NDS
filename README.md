# usugata PC

usugata PC (うすがた　ＰＣ) is a legit-use box for all your 'mons. It allows
you to easily store and move your collection between games and generations.
The current version only comes with Generation 1 support.

## Building

### Dependencies

#### Docker

The simplest method of installing this project's dependencies is to use the
docker image. Simply clone this project and run the following:

```
docker run --rm -itv /path/to/usugatapc:/work registry.gitlab.com/monlib/monman.nds/ci-image:9b7bc4
export CONAN_USER_HOME=/
init-conan-config
printf "[build_requires]\ncmake_installer/0.10.0@conan/stable\n" >> /.conan/profiles/monlib-base
cd /work
```

You will now be in the project's directory with all dependencies correctly set up. Proceed to the "Build" section and type `exit` once you are done.

#### Manual

If you don't want to use Docker, then you can manually install the required dependencies.
You will probably end up with different versions than what is tested against, so please report
incompatibilities.

If you are using Windows, then please use WSL or Ubuntu in a virtual machine (or docker).
Using the MSYS2 that is provided with the devkitPro installer requires you to manually hunt down
and compile some dependencies.

Firstly, install devkitARM and the required nds libraries by [installing devkitPro's pacman](https://devkitpro.org/wiki/devkitPro_pacman) and then installing the nds-dev group:

```sh
dkp-pacman -S nds-dev
```

After that, install the other dependencies through apt / your distro's package manager and pip.
You may use a virtualenv, in that case omit the `--user`.

```sh
sudo apt install python3-pip imagemagick
pip3 install --user conan Pillow docopt
```

Now you'll need to configure conan. If you already have a conan installation and don't want
to mess with it, then please look into `CONAN_USER_HOME`. You'll have to add the remotes
for monlib and bincrafters for conan to pull in the dependencies:

```sh
conan remote add monlib https://api.bintray.com/conan/monlib/Monlib
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan
```

Conan also needs some settings and profiles to work with the 3ds, so copy them over from
`monlib/conan-config`:

```sh
git clone https://gitlab.com/monlib/conan-config.git /tmp/conan-config
cp -r /conan-config/* ~/.conan
rm -rf /tmp/conan-config
```

You will also need access to cmake>=3.10.0. Either install it on your system (note that
package managers often have horribly outdated versions) or add the following lines
to your monlib-base profile, by editing ~/.conan/profiles/monlib-base:

```
[build_requires]
cmake_installer/3.10.0@conan/stable
```

Finally clone this project and cd into it. You are now all set up for compiling.

### Build

To compile everything, just type `make` and you'll get `usugataPC.nds` placed into your
project directory.

## Credits

- Dazz for ripping (most) icons up to gen4.
- KurainoOni for ripping icons for Platinum's special forms.
- Ploaj for ripping the gen5 monster icons.
- redblueyellow for ripping the B2/W2 box backgrounds.
- spaceemotion for ripping the Platinum box backgrounds.
- Christian Munk for his 7:12 Serif font, licensed under CC BY-SA 3.0.
