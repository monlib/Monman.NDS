# Text

## Introduction

Text rendering is going to be a PITA. This document serves as a technical
specification and scratchpad for implementation ideas.

## Requirements

The rendering engine must be able to display English, Japanese and custom
glyphs.

### Hard Goals

These features must be implemented and if they are not, the text engine is
not complete.

#### Support Variable-Width Fonts

Without variable width fonts, the bulky ロ would take up as much space as an i.
The main advantage of not implementing this feature would be the ability to use
simple tiled backgrounds for text display, however, this would limit each line
to containing either 32 or 16 characters at most. By implementing this feature,
usugata PC closely follows the example of all games from Gen3 onward.

#### Print to Tiles and Sprites

The need to print to tiles is obvious, as they excel at providing static
images. Note that due to variable width fonts there is rarely the option of
reusing the same tileset entries for multiple tilemap entries, thus this
optimization is not expected to take place.

Printing to sprites enables separate palette changes, animations,
transformations, etc. This also enables text to change without the background
it was written on having to be reloaded.

#### Limit Text Length

Both when printing to tiles and sprites, there should be a guarantee hat only
the designated regions of VRAM are written to.

#### Support UTF-8

Monlib uses UTF-8 to encode text, thus the encoding must be understood by the
text engine.

### Soft Goals

These goals are not easily measurable, but should still be taken into account
when designing an implementation.

#### Limit the RAM Footprint

The text engine should seek to exploit the NDS's hardware and features to
minimize it's footprint in main memory. This ensures that
platform-independent code has as much memory as needed.

### May Not Implement

This section serves to explictly state which features the rendering engine
doesn't need to (and thus won't) support.

#### Combining Characters

This includes ligatures, diacritical marks, skin color modifiers etc.
Supporting these characters would greatly complicate the conversion between
strings and glyphs due to it breaking the assumption that there is one glyph
per Unicode codepoint. In Gen1, there exists a precedent for such characters
(e.g. あ゛ is stored as 0x21), but later games use only those characters
present in Unicode. Printing two separate glyphs is good enough, especially
with a variable-width font.

#### Right-To-Left

RTL text greatly complicates text rendering and is not needed since no main
series game on the NDS handles RTL text.

## Implementation

This section discusses a possible implementation.

### UTF-8 To Glyph Indexes

Text is received as UTF-8 encoded strings which must be converted to glyph
indexes for later display. This process requires a lookup table, optimized for
fast retrieval. Because there is no need to add/remove glyphs or codepoints at
runtime, this information may be stored in a sorted array of integer tuples.

### Glyph Positioning

To position glyphs, the text engine must know each glyph's size, vertical
positioning and the spacing between this and the following glyph. These values
can be assumed to be in [0; 15] or [-8; 7], respectively. With this information,
the engine must translate the offsets into a position in VRAM, and apply a
bitmask to that position (it must operate on a u16 or u32, since u8 writes are
disallowed). It must also verify that the given position is within the area it
may draw to and discard any pixels that are outside of it.

### Storage

Since 16bit color sprites are generally not used, fonts and other data may be
stored in the upper 64kiB of OVRAM.

The UTF-8 to Index lookup table needs to map Unicode codepoints (21 bits) to
indexes which correspond to adresses in upper OVRAM (16 bits). These values may
be packed into three halfwords or two words. While three halfwords will be
smaller, applying bitshifts and masks to retrieve the values is slower than
working with native words. Unless memory becomes a pressing concern (unlikely
for less than 2^10 glyphs) the faster option should be preferred.

Each glyph in OVRAM will carry a header with information regarding it's size
and positioning. Since each of these values can be represented in 4 bits, a
single halfword suffices for this header. Due to alignment, there will be up to
one wasted byte preceding each header. This issue is not fixable without *lots*
of bitshifting. Following the header, a variable number of bytes is dedicated
to storing pixels.

The bitdepth may be as low as 1bpp, though visuals are likely to be poor. The
main series games have proven that three colors suffice for acceptable text
and thus 2bpp should be used, unless space becomes a pressing concern. Using
1.5bpp is also possible, but complicates access.

### Conversion

Since the user does not need to add new fonts at runtime, the required data
should be generated at compile time and included in the filesystem for later
loading into VRAM. A python script should transform a configuration file which
maps from UTF-8 to tiles and stores metadata and a directory of glyphs into a
binary file to be loaded at runtime. The script should be capable of deducing
height and width from the images and default the vertical offset and spacing
to zero.

ImageMagick may be used to split an image containing a glyph tileset into
individual glyphs, change the colors to predefined values and trim whitespace
around the images. Sadly, I cannot convince grit to use a common palette for
all glyphs without gluing them together in a giant tileset. I will have to
reimplement 2bpp packing in python.
